﻿namespace PhonemeMatrixTool
{
    partial class FormPhonemeMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CBInitLang = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CBTarLang = new System.Windows.Forms.ComboBox();
            this.TBPhoneme = new System.Windows.Forms.TextBox();
            this.ButtonInsert = new System.Windows.Forms.Button();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // CBInitLang
            // 
            this.CBInitLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBInitLang.FormattingEnabled = true;
            this.CBInitLang.Location = new System.Drawing.Point(116, 25);
            this.CBInitLang.Name = "CBInitLang";
            this.CBInitLang.Size = new System.Drawing.Size(121, 21);
            this.CBInitLang.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Initial Language:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Target Language:";
            // 
            // CBTarLang
            // 
            this.CBTarLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBTarLang.FormattingEnabled = true;
            this.CBTarLang.Location = new System.Drawing.Point(120, 52);
            this.CBTarLang.Name = "CBTarLang";
            this.CBTarLang.Size = new System.Drawing.Size(121, 21);
            this.CBTarLang.TabIndex = 3;
            // 
            // TBPhoneme
            // 
            this.TBPhoneme.Location = new System.Drawing.Point(12, 115);
            this.TBPhoneme.Multiline = true;
            this.TBPhoneme.Name = "TBPhoneme";
            this.TBPhoneme.Size = new System.Drawing.Size(236, 194);
            this.TBPhoneme.TabIndex = 4;
            // 
            // ButtonInsert
            // 
            this.ButtonInsert.Location = new System.Drawing.Point(94, 324);
            this.ButtonInsert.Name = "ButtonInsert";
            this.ButtonInsert.Size = new System.Drawing.Size(75, 23);
            this.ButtonInsert.TabIndex = 5;
            this.ButtonInsert.Text = "Insert";
            this.ButtonInsert.UseVisualStyleBackColor = true;
            this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
            // 
            // ButtonClear
            // 
            this.ButtonClear.Location = new System.Drawing.Point(94, 86);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(75, 23);
            this.ButtonClear.TabIndex = 6;
            this.ButtonClear.Text = "Clear";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormPhonemeMatrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 359);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.ButtonInsert);
            this.Controls.Add(this.TBPhoneme);
            this.Controls.Add(this.CBTarLang);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CBInitLang);
            this.Name = "FormPhonemeMatrix";
            this.Text = "Phoneme Matrix";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CBInitLang;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CBTarLang;
        private System.Windows.Forms.TextBox TBPhoneme;
        private System.Windows.Forms.Button ButtonInsert;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

