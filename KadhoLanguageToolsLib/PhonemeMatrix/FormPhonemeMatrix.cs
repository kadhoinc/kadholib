﻿using KadhoLanguageLib;
using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeMatrixTool
{
    public partial class FormPhonemeMatrix : Form
    {
        private List<Phoneme> ipaList;
        private List<KadhoLanguageLib.PhonemeMatrix> matrices;

        public FormPhonemeMatrix()
        {
            InitializeComponent();

            // setup language combo boxes
            for (LanguageType i = LanguageType.Mandarin; i < LanguageType.MaxType; i++)
            {
                CBInitLang.Items.Add(i.ToString());
                CBTarLang.Items.Add(i.ToString());
            }
            
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Title = "Select txt file of the IPA and ACII table";

            DialogResult result = openFileDialog1.ShowDialog();

            if (!result.ToString().Equals("OK"))
                return;

            // load ipa list 
            ipaList = IPAUtility.GetPhonemes(File.ReadAllText(openFileDialog1.FileName));
            
            // import matrices
            if (!File.Exists("phoneme_matrix.txt"))
                matrices = new List<PhonemeMatrix>();
            else
                matrices = IPAUtility.GetPhonemeMatrices(File.ReadAllText("phoneme_matrix.txt"), ipaList);
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            TBPhoneme.Text = string.Empty;
        }

        private void ButtonInsert_Click(object sender, EventArgs e)
        {
            LanguageType initType = (LanguageType)Enum.Parse(typeof(LanguageType), (string)CBInitLang.SelectedItem),
                targetType = (LanguageType)Enum.Parse(typeof(LanguageType), (string)CBTarLang.SelectedItem);

            string[] asciiValues = Regex.Split(TBPhoneme.Text.Replace('\t', ' '), " ");

            List<string> missingValues = new List<string>();

            KadhoLanguageLib.PhonemeMatrix newMatrix = new KadhoLanguageLib.PhonemeMatrix(initType, targetType);

            foreach (string i in asciiValues)
            {
                if (ipaList.Exists(a => a.ASCII == i))
                {
                    newMatrix.Phonemes.Add(ipaList.First(a => a.ASCII == i));
                }
                else
                    missingValues.Add(i);
            }

            if (missingValues.Count > 0)
                MessageBox.Show("The following ASCII values were not added:\n" + string.Join("\n", missingValues));

            if (matrices.Exists(a => a.InitialLanguage == newMatrix.InitialLanguage && a.TargetLanguage == newMatrix.TargetLanguage))
                matrices.First(a => a.InitialLanguage == newMatrix.InitialLanguage && a.TargetLanguage == newMatrix.TargetLanguage).Phonemes = newMatrix.Phonemes;
            else
                matrices.Add(newMatrix);

            // save the file
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(KadhoLanguageLib.PhonemeMatrixData[]));
            FileStream file = new FileStream("phoneme_matrix.txt", FileMode.OpenOrCreate);
            ser.WriteObject(file, matrices.Select(a => a.GetExportData()).ToArray());
            file.Close();
        }
    }
}
