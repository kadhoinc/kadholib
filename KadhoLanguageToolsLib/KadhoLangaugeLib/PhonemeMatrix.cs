﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace KadhoLanguageLib
{
    public class PhonemeMatrix
    {
        #region Properties

        /// <summary>
        /// Initial language
        /// </summary>
        public LanguageType InitialLanguage { get; set; }

        /// <summary>
        /// Target language
        /// </summary>
        public LanguageType TargetLanguage { get; set; }

        /// <summary>
        /// Phonemes for the matrix
        /// </summary>
        public List<Phoneme> Phonemes { get; set; }

        #endregion

        #region Constructor

        public PhonemeMatrix()
        {
            Phonemes = new List<Phoneme>();
        }

        public PhonemeMatrix(LanguageType initLang, LanguageType tarLang)
            : this()
        {
            InitialLanguage = initLang;
            TargetLanguage = tarLang;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get data for exporting to json
        /// </summary>
        /// <returns></returns>
        public PhonemeMatrixData GetExportData()
        {
            return new PhonemeMatrixData()
            {
                InitialLanguage = InitialLanguage,
                TargetLanguage = TargetLanguage,
                PhonemeIds = Phonemes.Select(i => i.Id).ToArray()
            };
        }

        /// <summary>
        /// Import data to the matrix.
        /// </summary>
        /// <param name="data"></param>
        public void ImportData(PhonemeMatrixData data, List<Phoneme> ipaList)
        {
            Phonemes.Clear();

            InitialLanguage = data.InitialLanguage;
            TargetLanguage = data.TargetLanguage;
            foreach (int id in data.PhonemeIds)
                Phonemes.Add(ipaList.First(a => a.Id == id));
        }

        #endregion
    }

    [DataContract]
    public class PhonemeMatrixData
    {
        /// <summary>
        /// Initial language
        /// </summary>
        [DataMember]
        public LanguageType InitialLanguage { get; set; }

        /// <summary>
        /// Target language
        /// </summary>
        [DataMember]
        public LanguageType TargetLanguage { get; set; }

        /// <summary>
        /// Phonemes for the matrix
        /// </summary>
        [DataMember]
        public int[] PhonemeIds { get; set; }
    }
}
