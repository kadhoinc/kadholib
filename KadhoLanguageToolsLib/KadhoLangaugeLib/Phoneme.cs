﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace KadhoLanguageLib
{
    [DataContract]
    public class Phoneme
    {
        #region Properties

        /// <summary>
        /// Phoneme Id
        /// </summary>
        [DataMember]
        public int Id { get; set; }
        
        /// <summary>
        /// IPA string value
        /// </summary>
        [DataMember]
        public string IPA { get; set; }

        /// <summary>
        /// ASCII value
        /// </summary>
        [DataMember]
        public string ASCII { get; set; }

        /// <summary>
        /// Phoneme category flags.
        /// </summary>
        public PhonemeCatogoryType PhonemeCategory { get; set; }

        #endregion

        #region Constructor

        public Phoneme()
        {

        }

        public Phoneme(Phoneme p)
        {
            Id = p.Id;
            IPA = p.IPA;
            ASCII = p.ASCII;
            PhonemeCategory = p.PhonemeCategory;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns true if the Phoneme has the category type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool HasPhonemeCategory(PhonemeCatogoryType type)
        {
            return (PhonemeCategory & type) == type;
        }

        /// <summary>
        /// Add the phoneme category type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Phoneme AddPhonemeCategory(PhonemeCatogoryType type)
        {
            PhonemeCategory |= type;
            return this;
        }

        /// <summary>
        /// Remove the phoneme catogory type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Phoneme RemovePhonemeCategory(PhonemeCatogoryType type)
        {
            PhonemeCategory &= ~type;
            return this;
        }

        #endregion

        #region Class override methods

        public override bool Equals(object obj)
        {
            if (obj is Phoneme)
                return ((Phoneme)obj).IPA == IPA;
            return false;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        #endregion
    }


}
