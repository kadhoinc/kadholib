﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KadhoLanguageLib
{
    public class PhonemeCategoryLibrary
    {
        private Dictionary<LanguageType, Dictionary<int, PhonemeCatogoryType>> lib;

        public PhonemeCategoryLibrary()
        {
            lib = new Dictionary<LanguageType, Dictionary<int, PhonemeCatogoryType>>();
        }

        #region Public methods

        /// <summary>
        /// Set the phoneme category for the given language and phoneme Id.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SetPhonemeCategory(LanguageType language, int phonemeId, PhonemeCatogoryType type)
        {
            if (!lib.ContainsKey(language))
                lib.Add(language, new Dictionary<int, PhonemeCatogoryType>());

            if (lib[language].ContainsKey(phonemeId))
                lib[language][phonemeId] = type;
            else
                lib[language].Add(phonemeId, type);

            return true;
        }

        /// <summary>
        /// Add the phoneme category for the given language and phoneme Id.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool AddPhonemeCategory(LanguageType language, int phonemeId, PhonemeCatogoryType type)
        {
            if (!lib.ContainsKey(language))
                lib.Add(language, new Dictionary<int, PhonemeCatogoryType>());

            if (lib[language].ContainsKey(phonemeId))
                lib[language][phonemeId] |= type;
            else
                lib[language].Add(phonemeId, type);

            return true;
        }

        /// <summary>
        /// Remove the phoneme category for the given language and phoneme Id.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool RemovePhonemeCategory(LanguageType language, int phonemeId, PhonemeCatogoryType type)
        {
            if (!lib.ContainsKey(language))
                return false;

            if (lib[language].ContainsKey(phonemeId))
            {
                lib[language][phonemeId] &= ~type;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get phoneme category given the language and phoneme Id.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <returns></returns>
        public PhonemeCatogoryType GetPhonemeCategory(LanguageType language, int phonemeId)
        {
            if (!lib.ContainsKey(language))
                return (PhonemeCatogoryType)0;

            if (lib[language].ContainsKey(phonemeId))
                return lib[language][phonemeId];

            return (PhonemeCatogoryType)0;
        }

        /// <summary>
        /// Check if the phoneme category exists for the given langauge and phoneme Id.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <returns></returns>
        public bool HasPhonemeCategory(LanguageType language, int phonemeId)
        {
            if (!lib.ContainsKey(language))
                return false;

            if (lib[language].ContainsKey(phonemeId))
                return true;

            return false;
        }

        /// <summary>
        /// Get the dictionary of phoneme Ids and their PhonemeCategoryType.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<int, PhonemeCatogoryType> GetPhonemeCategoryDictionary(LanguageType language)
        {
            if (!lib.ContainsKey(language))
                return null;

            return lib[language];
        }

        /// <summary>
        /// Get an array of PhonemeCategoryType flags separated into an array.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="phonemeId"></param>
        /// <returns></returns>
        public PhonemeCatogoryType[] GetPhonemeCategoryArray(LanguageType language, int phonemeId)
        {
            PhonemeCatogoryType p = GetPhonemeCategory(language, phonemeId);
            return GetPhonemeCategoryArray(p);
        }

        /// <summary>
        /// Get a PhonemeCategoryType array from the PhonemeCategoryType flags.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static PhonemeCatogoryType[] GetPhonemeCategoryArray(PhonemeCatogoryType type)
        {
            List<PhonemeCatogoryType> pList = new List<PhonemeCatogoryType>();
            foreach (PhonemeCatogoryType i in Enum.GetValues(typeof(PhonemeCatogoryType)))
                if ((type & i) == i)
                    pList.Add(i);
            return pList.ToArray();
        }


        /// <summary>
        /// Load language specific phoneme categories for the list of phonemes.
        /// </summary>
        /// <param name="phonemes"></param>
        /// <param name="language"></param>
        public void LoadCategoriesForPhonemes(List<Phoneme> phonemes, LanguageType language)
        {
            foreach (Phoneme p in phonemes)
                p.PhonemeCategory = GetPhonemeCategory(language, p.Id);
        }

        /// <summary>
        /// Get export data as a list of PhonemeCategoryData.
        /// </summary>
        /// <returns></returns>
        public PhonemeCategoryData[] GetExportData()
        {
            List<PhonemeCategoryData> export = new List<PhonemeCategoryData>();
            foreach (KeyValuePair<LanguageType, Dictionary<int, PhonemeCatogoryType>> a in lib)
                foreach (KeyValuePair<int, PhonemeCatogoryType> b in a.Value)
                    export.Add(new PhonemeCategoryData(b.Key, b.Value, a.Key));

            return export.ToArray();
        }

        /// <summary>
        /// Get export data as a list of PhonemeCategoryData for the specific language.
        /// </summary>
        /// <returns></returns>
        public PhonemeCategoryData[] GetExportData(LanguageType language)
        {
            List<PhonemeCategoryData> export = new List<PhonemeCategoryData>();
            if (lib.ContainsKey(language))
                foreach (KeyValuePair<int, PhonemeCatogoryType> b in lib[language])
                    export.Add(new PhonemeCategoryData(b.Key, b.Value, language));

            return export.ToArray();
        }


        /// <summary>
        /// Import data from list of PhonemeCategoryData.
        /// </summary>
        /// <param name="data"></param>
        public void ImportData(IEnumerable<PhonemeCategoryData> data)
        {
            foreach (PhonemeCategoryData i in data)
            {
                if (!lib.ContainsKey(i.Language))
                    lib.Add(i.Language, new Dictionary<int, PhonemeCatogoryType>());
                lib[i.Language][i.Id] = (PhonemeCatogoryType)i.PhonemeCategories;
            }
        }

        #endregion
    }

    [DataContract]
    public class PhonemeCategoryData
    {
        /// <summary>
        /// Phoneme Id
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Bitwise flags of phoneme categories
        /// </summary>
        [DataMember]
        public ulong PhonemeCategories { get; set; }

        /// <summary>
        /// Language of the data.
        /// </summary>
        [DataMember]
        public LanguageType Language { get; set; }

        #region Constructor

        public PhonemeCategoryData()
        {

        }

        public PhonemeCategoryData(int id, PhonemeCatogoryType type, LanguageType language)
        {
            Id = id;
            PhonemeCategories = (ulong)type;
            Language = language;
        }

        #endregion
    }
}
