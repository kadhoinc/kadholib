﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KadhoLanguageLib
{
    public class LanguageLearner
    {
        public enum LanguageDifficulty
        {
            /// <summary>
            /// Sounds only
            /// </summary>
            Level1,
            /// <summary>
            /// Sounds and syllables
            /// </summary>
            Level2,
            /// <summary>
            /// Sounds, syllables, and words
            /// </summary>
            Level3
        }

        #region Fields

        private List<LearnObject> learnList;
        private List<Phoneme> phonemes;
        private SylTypeDifficultyLibrary sylDifficulty;
        private List<WordObject> words;
        private PhonemeCategoryLibrary phonemeCategoryLib;
        private Random random;

        #endregion

        #region Properties

        public LanguageType InitialLanguage { get; set; }

        public LanguageType TargetLanguage { get; set; }

        public LanguageDifficulty Difficulty { get; set; }

        #endregion

        public LanguageLearner(LanguageType initLang, LanguageType tarLang, 
            string phonemeJson, string sylDifficultyJson, string wordsJson, string phonemeCategoryJson)
        {
            InitialLanguage = initLang;
            TargetLanguage = tarLang;
            phonemes = IPAUtility.GetPhonemes(phonemeJson);
            sylDifficulty = IPAUtility.GetSylTypeDifficulty(sylDifficultyJson);
            words = IPAUtility.GetWords(wordsJson, phonemes);
            phonemeCategoryLib = new PhonemeCategoryLibrary();
            
            // load phoneme category library
            phonemeCategoryLib.ImportData(IPAUtility.GetPhonemeCategoryData(phonemeCategoryJson));
            
            random = new Random();
        }

        #region Public methods

        /// <summary>
        /// Get the next 
        /// </summary>
        /// <returns></returns>
        public LearnObject GetNext()
        {
            if (learnList.Count == 0)
                return null;

            // get the top rank LearnObject
            LearnObject top = learnList.OrderByDescending(a => a.Rank).First();

            // get similar rank LearnObjects
            LearnObject[] selectedList = learnList.Where(a => a.Rank == top.Rank).ToArray();

            // select random LearnObject from the filtered list
            return selectedList[random.Next(0, selectedList.Length)];
        }

        /// <summary>
        /// Get an array of LearnObjects, containing one answer mixed randomly with 
        /// other LearnObjects.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="amount">total shuffled answers</param>
        /// <returns></returns>
        public LearnObject[] GetShuffledAnswer(LearnObject answer, int amount)
        {
            List<LearnObject> possibleAnswers = new List<LearnObject>();

            while (amount > 1)
            {
                LearnObject temp = learnList[random.Next(0, learnList.Count)];
                if (temp != answer && !possibleAnswers.Contains(temp))
                {
                    possibleAnswers.Add(temp);
                    amount--;
                }
            }

            possibleAnswers.Insert(random.Next(0, possibleAnswers.Count + 1), answer);

            return possibleAnswers.ToArray();
        }
        
        public List<LearnObject>.Enumerator GetLearnList()
        {
            return learnList.GetEnumerator();
        }

        /// <summary>
        /// Get the list of phonemes for the language.
        /// </summary>
        public List<Phoneme> GetLanguagePhonemes(LanguageType language)
        {
            List<Phoneme> phonemesA = new List<Phoneme>();
            Dictionary<int, PhonemeCatogoryType> phonemeCategories = phonemeCategoryLib.GetPhonemeCategoryDictionary(language);

            if (phonemeCategories != null)
                foreach (Phoneme p in phonemes)
                {
                    // phoneme exists for the language
                    if (phonemeCategories.ContainsKey(p.Id))
                    {
                        Phoneme a = new Phoneme(p);
                        a.PhonemeCategory = phonemeCategories[p.Id];
                        phonemesA.Add(a);
                    }
                }

            return phonemesA;
        }

        /// <summary>
        /// Return true if all LearnObjects have reached rank 0 (finished).
        /// </summary>
        /// <returns></returns>
        public bool FinishedLearnList()
        {
            return learnList.Where(a => a.Rank > 0).Count() <= 0;
        }

        /// <summary>
        /// Setup the learner for phonemes.
        /// </summary>
        public void SetupPhonemesLearner()
        {
            learnList = new List<LearnObject>();

            // init phonemes specific for the target language
            List<Phoneme> targetLanguagePhonemes = GetLanguagePhonemes(TargetLanguage);            

            foreach (Phoneme p in targetLanguagePhonemes)
            {
                learnList.Add(
                    new PhonemeLearnObject(1, -1, 0)
                    {
                        Phoneme = p
                    });
            }
        }

        /// <summary>
        /// Setup the learner for syllables from the word list.
        /// </summary>
        public void SetupSyllablesLearner()
        {
            learnList = new List<LearnObject>();

            // get unique syllables from words
            int maxDifficulty = 0;
            HashSet<string> syllables = new HashSet<string>();
            foreach (WordObject w in words)
                for (int i = 0; i < w.Syllables.Length; i++)
                    if (!syllables.Contains(w.Syllables[i]))
                    {
                        // keep track of seen syllables
                        syllables.Add(w.Syllables[i]);

                        int difficulty = sylDifficulty.GetSylTypeDifficulty(TargetLanguage, w.SylTypes[i]);
                        if (difficulty == 0)
                            continue;
                        if (maxDifficulty < difficulty)
                            maxDifficulty = difficulty;
                        learnList.Add(
                            new SyllableLearnObject(difficulty, -1, 0)
                            {
                                Syllable = w.Syllables[i],
                                EnglishWord = w.EnglishWord
                            });
                    }

            // normalize learn list using the ranks
            double ratio = learnList.Count / (double)maxDifficulty;

            foreach (SyllableLearnObject s in learnList)
            {
                double normalizedRank = s.Rank * ratio;
                // previously, rank was based on difficulty. reverse difficulty to rank easy ones higher.
                int rank = learnList.Count + 1 - (int)normalizedRank;
                s.Rank = rank;
            }
        }

        /// <summary>
        /// Setup learner for words.
        /// </summary>
        public void SetupWordsLearner()
        {
            learnList = new List<LearnObject>();
            int maxDifficulty = 0;

            foreach (WordObject w in words)
            {
                int difficulty = sylDifficulty.GetTotalSylTypeDifficulty(TargetLanguage, w.SylTypes);
                if (maxDifficulty < difficulty)
                    maxDifficulty = difficulty;
                learnList.Add(
                    new WordLearnObject(difficulty, -1, 0)
                    {
                        Word = w
                    });
            }

            // normalize learn list using the ranks
            double ratio = learnList.Count / (double)maxDifficulty;
            foreach (WordLearnObject w in learnList)
            {
                double normalizedRank = w.Rank * ratio;
                // previously, rank was based on difficulty. reverse difficulty to rank easy ones higher.
                int rank = learnList.Count + 1 - (int)normalizedRank;
                w.Rank = rank;
            }
        }

        #endregion
    }

    public class LearnObject
    {
        #region Properties

        /// <summary>
        /// Rank of the object. Higher rank means it is higher priority.
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Number of correct answers.
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// Number of incorrect answers.
        /// </summary>
        public int IncorrectCount { get; set; }

        /// <summary>
        /// Amount to change rank on correct answer.
        /// </summary>
        public int CorrectRankChange { get; set; }

        /// <summary>
        /// Amount to change rank on incorrect answer.
        /// </summary>
        public int IncorrectRankChange { get; set; }

        #endregion

        public LearnObject(int rank, int correctRankChange, int incorrectRankChange)
        {
            Rank = rank;
            CorrectRankChange = correctRankChange;
            IncorrectRankChange = incorrectRankChange;
            CorrectCount = 0;
            IncorrectCount = 0;
        }

        /// <summary>
        /// Get the correct / total ratio.
        /// </summary>
        /// <returns></returns>
        public double GetCorrectRatio()
        {
            if (CorrectCount + IncorrectCount == 0)
                return 0;

            return CorrectCount / (CorrectCount + IncorrectCount);
        }

        /// <summary>
        /// Add incorrect count and update ranks.
        /// </summary>
        public void AddIncorrectCount()
        {
            IncorrectCount++;
            Rank += IncorrectRankChange;
            if (Rank < 0)
                Rank = 0;
        }

        /// <summary>
        /// Add correct count and update ranks.
        /// </summary>
        public void AddCorrectCount()
        {
            CorrectCount++;
            Rank += CorrectRankChange;
            if (Rank < 0)
                Rank = 0;
        }
    }
    
    public class PhonemeLearnObject : LearnObject
    {
        public Phoneme Phoneme { get; set; }


        public PhonemeLearnObject(int rank, int correctRankChange, int incorrectRankChange)
            : base(rank, correctRankChange, incorrectRankChange)
        {

        }
    }

    public class SyllableLearnObject : LearnObject
    {
        /// <summary>
        /// Syllable
        /// </summary>
        public string Syllable { get; set; }

        /// <summary>
        /// English word the syllable came from
        /// </summary>
        public string EnglishWord { get; set; }

        public SyllableLearnObject(int rank, int correctRankChange, int incorrectRankChange)
            : base(rank, correctRankChange, incorrectRankChange)
        {

        }
    }

    public class WordLearnObject : LearnObject
    {
        public WordObject Word { get; set; }

        public WordLearnObject(int rank, int correctRankChange, int incorrectRankChange)
            : base(rank, correctRankChange, incorrectRankChange)
        {

        }
    }
}
