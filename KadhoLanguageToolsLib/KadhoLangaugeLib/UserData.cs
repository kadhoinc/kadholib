﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KadhoLanguageLib
{
    public class UserData
    {
        /// <summary>
        /// User Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string Name { get; set; }

        public LanguageType InitLanguage { get; set; }

        public LanguageType TargetLanguage { get; set; }
    }
}
