﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace KadhoLanguageLib
{
    public class SylTypeDifficultyLibrary
    {
        #region Fields

        private Dictionary<LanguageType, Dictionary<string, int>> sylDictionary;

        #endregion

        #region Constructor

        public SylTypeDifficultyLibrary()
        {
            sylDictionary = new Dictionary<LanguageType, Dictionary<string, int>>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the dictionary of syl type difficulty for the language. Return null if not found.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<string, int> GetSylTypeDifficulty(LanguageType language)
        {
            if (!sylDictionary.ContainsKey(language))
                return null;
            return sylDictionary[language];
        }

        /// <summary>
        /// Add the syl types for the language.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="sylTypes"></param>
        public void AddUpdate(LanguageType language, Dictionary<string, int> sylTypes)
        {
            sylDictionary[language] = sylTypes;
        }

        /// <summary>
        /// Add the list of syl types for the language, ordered first to last in difficulty.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="sylTypes"></param>
        public void Add(LanguageType language, IList<string> sylTypes)
        {
            Dictionary<string, int> newTypes = new Dictionary<string, int>();

            int maxDiff = sylTypes.Count;
            for (int i = 0; i < sylTypes.Count; i++)
                newTypes.Add(sylTypes[i], maxDiff - i);

            AddUpdate(language, newTypes);
        }

        /// <summary>
        /// Get the syl type difficulty level. Returns 0 if not found.
        /// Difficulty is ranked starting from 1, increasing in difficulty.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="sylType"></param>
        /// <returns></returns>
        public int GetSylTypeDifficulty(LanguageType language, string sylType)
        {
            // language does not exist
            if (!sylDictionary.ContainsKey(language))
                return 0;

            // syl type does not exist
            if (!sylDictionary[language].ContainsKey(sylType))
                return 0;

            return sylDictionary[language][sylType];
        }

        /// <summary>
        /// Get the total syl type difficulty for the array of syl types.
        /// </summary>
        /// <param name="?"></param>
        /// <param name="sylTypes"></param>
        /// <returns></returns>
        public int GetTotalSylTypeDifficulty(LanguageType language, string[] sylTypes)
        {
            int difficulty = 0;

            foreach (string s in sylTypes)
                difficulty += GetSylTypeDifficulty(language, s);

            return difficulty;
        }

        /// <summary>
        /// Get the exported data for json.
        /// </summary>
        /// <returns></returns>
        public SylTypeDifficultyData[] GetExportData()
        {
            List<SylTypeDifficultyData> export = new List<SylTypeDifficultyData>();
            foreach (KeyValuePair<LanguageType, Dictionary<string, int>> kvp in sylDictionary)
            {
                LanguageType lang = kvp.Key;
                Dictionary<string, int> difficulties = kvp.Value;
                SylTypeDifficultyData data = new SylTypeDifficultyData()
                {
                    Language = lang,
                    SylTypes = difficulties.OrderByDescending(a => a.Value).Select(a => a.Key).ToList()
                };
                export.Add(data);
            }
            return export.ToArray();
        }

        /// <summary>
        /// Import data
        /// </summary>
        /// <param name="data"></param>
        public void ImportData(IList<SylTypeDifficultyData> data)
        {
            if (data == null)
                return;

            foreach (SylTypeDifficultyData i in data)
                Add(i.Language, i.SylTypes);            
        }

        #endregion

    }

    [DataContract]
    public class SylTypeDifficultyData
    {
        /// <summary>
        /// Language
        /// </summary>
        [DataMember]
        public LanguageType Language { get; set; }

        /// <summary>
        /// List of syl types ordered from first to last difficulty.
        /// </summary>
        [DataMember]
        public List<string> SylTypes { get; set; }
    }
}
