﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KadhoLanguageLib
{
    public static class IPAUtility
    {
        /// <summary>
        /// Get the IPA object list from the json.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Phoneme> GetPhonemes(string json)
        {
            if (string.IsNullOrEmpty(json))
                return new List<Phoneme>();

            JsonReader reader = new JsonReader(json);
            return new List<Phoneme>(reader.Deserialize<Phoneme[]>());
        }

        /// <summary>
        /// Get the phoneme matrices from the json and ipa list.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="ipaList"></param>
        /// <returns></returns>
        public static List<PhonemeMatrix> GetPhonemeMatrices(string json, List<Phoneme> ipaList)
        {
            if (string.IsNullOrEmpty(json))
                return new List<PhonemeMatrix>();

            JsonReader reader = new JsonReader(json);
            PhonemeMatrixData[] matricesData = reader.Deserialize<PhonemeMatrixData[]>();
            List<PhonemeMatrix> matrices = new List<PhonemeMatrix>();
            foreach (PhonemeMatrixData data in matricesData)
            {
                PhonemeMatrix matrix = new PhonemeMatrix();
                matrix.ImportData(data, ipaList);
                matrices.Add(matrix);
            }
            return matrices;
        }

        /// <summary>
        /// Get the array of syl type difficulty data from the json.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static SylTypeDifficultyData[] GetSylTypeDifficultyData(string json)
        {
            if (string.IsNullOrEmpty(json))
                return null;

            JsonReader reader = new JsonReader(json);
            return reader.Deserialize<SylTypeDifficultyData[]>();
        }

        /// <summary>
        /// Get the syl type diffuclty from the json.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static SylTypeDifficultyLibrary GetSylTypeDifficulty(string json)
        {
            SylTypeDifficultyLibrary sylTypeDifficulty = new SylTypeDifficultyLibrary();
            if (!string.IsNullOrEmpty(json))
                sylTypeDifficulty.ImportData(IPAUtility.GetSylTypeDifficultyData(json));
            return sylTypeDifficulty;
        }

        public static PhonemeCategoryData[] GetPhonemeCategoryData(string json)
        {
            if (string.IsNullOrEmpty(json))
                return null;

            JsonReader reader = new JsonReader(json);
            return reader.Deserialize<PhonemeCategoryData[]>();
        }

        public static List<WordObject> GetWords(string json, List<Phoneme> phonemes)
        {
            if (string.IsNullOrEmpty(json))
                return new List<WordObject>();

            JsonReader reader = new JsonReader(json);
            WordObjectData[] data = reader.Deserialize<WordObjectData[]>();
            List<WordObject> words = new List<WordObject>();
            foreach (WordObjectData d in data)
            {
                WordObject w = new WordObject();
                w.ImportData(d, phonemes);
                words.Add(w);
            }
            return words;
        }
    }
}
