﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace KadhoLanguageLib
{
    public enum LanguageType
    {
        Mandarin = 1,
        English,
        Hindi,
        Japanese,
        Korean,
        French, 
        German,
        Spanish,
        Russian,
        Bengali,
        Arabic,
        Malaysian,
        Portuguese,
        Persian,
        Italian,

        MaxType
    }

    [Flags]
    public enum PhonemeCatogoryType : ulong
    {
        Stops = 1,
        Affricates = 2,
        Fricatives = 4,
        Sonorants = 8,
        Aspirated = 16,
        Unaspirated = 32,
        Interdental = 64,
        Voiced = 128,
        Nasal = 256,
        NonNasalSonorants = 512,
        AspiratedVoiced = 1024,
        UnaspiratedVoiced = 2048,
        Liquids = 4096,
        Approximant = 8192,
        StrongConsonants = 16384,
        AspiratedConsonants = 32768,
        UnaspiratedConsonants = 65536,
        VoicedFricatives = 131072,
        SoftConsonants = 262144,
        AspiratedVoiceless = 524288,
        UnaspiratedVoiceless = 1048576,
        Retroflex = 2097152,
        Pharyngeals = 4194304,
        Emphatics = 8388608,
        FrontVowels = 16777216,
        CentralVowels = 33554432,
        BackVowels = 67108864,
        CloseVowels = 134217728,
        MidVowels = 268435456,
        OpenVowels = 536870912,
        LaxVowels = 1073741824,
        LongVowels = 2147483648,
        ShortVowels = 4294967296,
        FrontRoundedVowels = 8589934592,
        NasalVowels = 17179869184,
        
        //MaxType = 17179869184
    }
}
