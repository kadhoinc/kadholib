﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text.RegularExpressions;

namespace KadhoLanguageLib
{
    public class WordObject
    {
        #region Properties

        /// <summary>
        /// Word in original language.
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// English translation of the word.
        /// </summary>
        public string EnglishWord { get; set; }

        /// <summary>
        /// Syllables of word.
        /// </summary>
        public string[] Syllables { get; set; }

        /// <summary>
        /// Phonemes separated by syllables.
        /// </summary>
        public Phoneme[][] PhonemeSyllables { get; set; }

        /// <summary>
        /// SylType separated by syllables.
        /// </summary>
        public string[] SylTypes { get; set; }

        #endregion

        #region Constructor

        #endregion

        #region Public methods

        /// <summary>
        /// Get data for exporting.
        /// </summary>
        /// <returns></returns>
        public WordObjectData GetExportData()
        {
            return new WordObjectData()
            {
                Word = Word,
                EnglishWord = EnglishWord,
                Syllables = Syllables,
                PhonemeSyllables = PhonemeSyllables.Select(a => a.Select(b => b.Id).ToArray()).ToArray(),
                SylTypes = SylTypes
            };
        }

        public void ImportData(WordObjectData data, List<Phoneme> phonemes)
        {
            Word = data.Word;
            EnglishWord = data.EnglishWord;
            Syllables = data.Syllables;
            SylTypes = data.SylTypes;

            PhonemeSyllables = new Phoneme[data.PhonemeSyllables.Length][];
            for (int i = 0; i < data.PhonemeSyllables.Length; i++)
            {
                List<Phoneme> p = new List<Phoneme>();
                foreach (int id in data.PhonemeSyllables[i])
                {
                    p.Add(phonemes.First(a => a.Id == id));
                }
                PhonemeSyllables[i] = p.ToArray();
            }
        }

        #endregion

        #region Public static methods

        /// <summary>
        /// Extract syllables from the string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string[] GetSyllables(string s)
        {
            return Regex.Split(s, " ");
        }

        /// <summary>
        /// Extract SylTypes from the string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string[] GetSylTypes(string s)
        {
            return Regex.Split(s, " ");
        }

        /// <summary>
        /// Extract phonemes syllables from the string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Phoneme[][] GetPhonemes(string s, List<Phoneme> phonemes)
        {
            string[] syllables = Regex.Split(s, "'").Select(a => a.Trim()).ToArray();
            List<Phoneme[]> tempA = new List<Phoneme[]>();
            foreach (string i in syllables)
            {
                List<Phoneme> tempB = new List<Phoneme>();
                string[] p = Regex.Split(i, " ");
                foreach (string k in p)
                {
                    tempB.Add(phonemes.First(a => a.ASCII == k));
                }
                tempA.Add(tempB.ToArray());
            }
            return tempA.ToArray();
        }

        #endregion
    }

    [DataContract]
    public class WordObjectData
    {
        /// <summary>
        /// Word in original language.
        /// </summary>
        [DataMember]
        public string Word { get; set; }

        /// <summary>
        /// English translation of the word.
        /// </summary>
        [DataMember]
        public string EnglishWord { get; set; }

        /// <summary>
        /// Syllables of word.
        /// </summary>
        [DataMember]
        public string[] Syllables { get; set; }

        /// <summary>
        /// Phoneme IDs separated by syllables.
        /// </summary>
        [DataMember]
        public int[][] PhonemeSyllables { get; set; }

        /// <summary>
        /// SylType separated by syllables.
        /// </summary>
        [DataMember]
        public string[] SylTypes { get; set; }
    }
}
