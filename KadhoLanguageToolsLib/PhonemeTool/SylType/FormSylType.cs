﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool
{
    public partial class FormSylType : Form
    {
        private SylTypeDifficultyLibrary sylTypeDifficulty;

        public FormSylType()
        {
            InitializeComponent();

            // setup language combo boxes
            for (LanguageType i = LanguageType.Mandarin; i < LanguageType.MaxType; i++)
            {
                CBLang.Items.Add(i.ToString());
            }

            if (File.Exists(Globals.SylTypesFile))
                sylTypeDifficulty = IPAUtility.GetSylTypeDifficulty(File.ReadAllText(Globals.SylTypesFile));
            else
                sylTypeDifficulty = new SylTypeDifficultyLibrary();
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            TBSyl.Text = string.Empty;
        }

        private void ButtonInsert_Click(object sender, EventArgs e)
        {
            LanguageType langType = (LanguageType)Enum.Parse(typeof(LanguageType), (string)CBLang.SelectedItem);

            string[] split = Regex.Split(TBSyl.Text, "\r\n");

            List<string> syllables = new List<string>();
            foreach (string s in split)
                if (!string.IsNullOrWhiteSpace(s))
                    syllables.Add(s);

            sylTypeDifficulty.Add(langType, syllables);

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SylTypeDifficultyData[]));
            FileStream file = new FileStream(Globals.SylTypesFile, FileMode.OpenOrCreate);
            ser.WriteObject(file, sylTypeDifficulty.GetExportData());
            file.Close();
        }

        private void ButtonSylTable_Click(object sender, EventArgs e)
        {
            FormViewSylTable formView = new FormViewSylTable(sylTypeDifficulty);
            formView.Show();
        }
    }
}
