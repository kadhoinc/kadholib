﻿namespace PhonemeTool
{
    partial class FormViewSylTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVTable = new System.Windows.Forms.DataGridView();
            this.Difficulty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SylType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CBLanguage = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGVTable)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVTable
            // 
            this.DGVTable.AllowUserToAddRows = false;
            this.DGVTable.AllowUserToDeleteRows = false;
            this.DGVTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Difficulty,
            this.SylType});
            this.DGVTable.Location = new System.Drawing.Point(11, 57);
            this.DGVTable.Name = "DGVTable";
            this.DGVTable.ReadOnly = true;
            this.DGVTable.Size = new System.Drawing.Size(282, 362);
            this.DGVTable.TabIndex = 0;
            // 
            // Difficulty
            // 
            this.Difficulty.HeaderText = "Difficulty";
            this.Difficulty.Name = "Difficulty";
            this.Difficulty.ReadOnly = true;
            // 
            // SylType
            // 
            this.SylType.HeaderText = "Syl Type";
            this.SylType.Name = "SylType";
            this.SylType.ReadOnly = true;
            // 
            // CBLanguage
            // 
            this.CBLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBLanguage.FormattingEnabled = true;
            this.CBLanguage.Location = new System.Drawing.Point(92, 25);
            this.CBLanguage.Name = "CBLanguage";
            this.CBLanguage.Size = new System.Drawing.Size(121, 21);
            this.CBLanguage.TabIndex = 1;
            this.CBLanguage.SelectedIndexChanged += new System.EventHandler(this.CBLanguage_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Language:";
            // 
            // FormViewSylTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 426);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CBLanguage);
            this.Controls.Add(this.DGVTable);
            this.Name = "FormViewSylTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Syl Table";
            ((System.ComponentModel.ISupportInitialize)(this.DGVTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Difficulty;
        private System.Windows.Forms.DataGridViewTextBoxColumn SylType;
        private System.Windows.Forms.ComboBox CBLanguage;
        private System.Windows.Forms.Label label1;
    }
}