﻿namespace PhonemeTool
{
    partial class FormSylType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CBLang = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.TBSyl = new System.Windows.Forms.TextBox();
            this.ButtonInsert = new System.Windows.Forms.Button();
            this.ButtonSylTable = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CBLang
            // 
            this.CBLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBLang.FormattingEnabled = true;
            this.CBLang.Location = new System.Drawing.Point(114, 22);
            this.CBLang.Name = "CBLang";
            this.CBLang.Size = new System.Drawing.Size(121, 21);
            this.CBLang.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Language:";
            // 
            // ButtonClear
            // 
            this.ButtonClear.Location = new System.Drawing.Point(110, 57);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(75, 23);
            this.ButtonClear.TabIndex = 9;
            this.ButtonClear.Text = "Clear";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // TBSyl
            // 
            this.TBSyl.Location = new System.Drawing.Point(28, 86);
            this.TBSyl.Multiline = true;
            this.TBSyl.Name = "TBSyl";
            this.TBSyl.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBSyl.Size = new System.Drawing.Size(236, 194);
            this.TBSyl.TabIndex = 7;
            // 
            // ButtonInsert
            // 
            this.ButtonInsert.Location = new System.Drawing.Point(33, 302);
            this.ButtonInsert.Name = "ButtonInsert";
            this.ButtonInsert.Size = new System.Drawing.Size(75, 23);
            this.ButtonInsert.TabIndex = 8;
            this.ButtonInsert.Text = "Insert";
            this.ButtonInsert.UseVisualStyleBackColor = true;
            this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
            // 
            // ButtonSylTable
            // 
            this.ButtonSylTable.Location = new System.Drawing.Point(189, 302);
            this.ButtonSylTable.Name = "ButtonSylTable";
            this.ButtonSylTable.Size = new System.Drawing.Size(75, 23);
            this.ButtonSylTable.TabIndex = 10;
            this.ButtonSylTable.Text = "Syl Table";
            this.ButtonSylTable.UseVisualStyleBackColor = true;
            this.ButtonSylTable.Click += new System.EventHandler(this.ButtonSylTable_Click);
            // 
            // FormSylType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 337);
            this.Controls.Add(this.ButtonSylTable);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.ButtonInsert);
            this.Controls.Add(this.TBSyl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CBLang);
            this.Name = "FormSylType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load Syl Types";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CBLang;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.TextBox TBSyl;
        private System.Windows.Forms.Button ButtonInsert;
        private System.Windows.Forms.Button ButtonSylTable;
    }
}

