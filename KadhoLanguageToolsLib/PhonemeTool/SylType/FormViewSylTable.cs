﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool
{
    public partial class FormViewSylTable : Form
    {
        private SylTypeDifficultyLibrary sylTypeDifficulty;

        public FormViewSylTable(SylTypeDifficultyLibrary sylTypeDifficulty)
        {
            InitializeComponent();
            this.sylTypeDifficulty = sylTypeDifficulty;


            // setup language combo boxes
            for (LanguageType i = LanguageType.Mandarin; i < LanguageType.MaxType; i++)
            {
                CBLanguage.Items.Add(i.ToString());
            }
        }

        private void CBLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            DGVTable.Rows.Clear();
            LanguageType langType = (LanguageType)Enum.Parse(typeof(LanguageType), (string)CBLanguage.SelectedItem);

            Dictionary<string, int> difficultyRanks = sylTypeDifficulty.GetSylTypeDifficulty(langType);
            if (difficultyRanks == null)
            {
                MessageBox.Show("The selected language has no syl types.");
                return;
            }

            foreach (KeyValuePair<string, int> kvp in difficultyRanks)
                DGVTable.Rows.Add(new object[] { kvp.Value, kvp.Key });
        }
    }
}
