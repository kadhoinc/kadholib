﻿namespace PhonemeTool
{
    partial class FormPhonemeCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DDLLanguage = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TBASCII = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DDLCategory = new System.Windows.Forms.ComboBox();
            this.ButtonInsert = new System.Windows.Forms.Button();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ButtonViewData = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DDLLanguage
            // 
            this.DDLLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DDLLanguage.FormattingEnabled = true;
            this.DDLLanguage.Location = new System.Drawing.Point(101, 17);
            this.DDLLanguage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DDLLanguage.Name = "DDLLanguage";
            this.DDLLanguage.Size = new System.Drawing.Size(156, 21);
            this.DDLLanguage.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Language:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TBASCII);
            this.groupBox1.Location = new System.Drawing.Point(6, 85);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(280, 240);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ASCII";
            // 
            // TBASCII
            // 
            this.TBASCII.Location = new System.Drawing.Point(3, 16);
            this.TBASCII.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TBASCII.Multiline = true;
            this.TBASCII.Name = "TBASCII";
            this.TBASCII.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBASCII.Size = new System.Drawing.Size(276, 223);
            this.TBASCII.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Category:";
            // 
            // DDLCategory
            // 
            this.DDLCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DDLCategory.FormattingEnabled = true;
            this.DDLCategory.Location = new System.Drawing.Point(97, 45);
            this.DDLCategory.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DDLCategory.Name = "DDLCategory";
            this.DDLCategory.Size = new System.Drawing.Size(156, 21);
            this.DDLCategory.TabIndex = 3;
            // 
            // ButtonInsert
            // 
            this.ButtonInsert.Location = new System.Drawing.Point(116, 394);
            this.ButtonInsert.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonInsert.Name = "ButtonInsert";
            this.ButtonInsert.Size = new System.Drawing.Size(60, 26);
            this.ButtonInsert.TabIndex = 5;
            this.ButtonInsert.Text = "Insert";
            this.ButtonInsert.UseVisualStyleBackColor = true;
            this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
            // 
            // ButtonClear
            // 
            this.ButtonClear.Location = new System.Drawing.Point(6, 328);
            this.ButtonClear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(71, 26);
            this.ButtonClear.TabIndex = 6;
            this.ButtonClear.Text = "Clear";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ButtonViewData
            // 
            this.ButtonViewData.Location = new System.Drawing.Point(226, 334);
            this.ButtonViewData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonViewData.Name = "ButtonViewData";
            this.ButtonViewData.Size = new System.Drawing.Size(58, 36);
            this.ButtonViewData.TabIndex = 7;
            this.ButtonViewData.Text = "View Data";
            this.ButtonViewData.UseVisualStyleBackColor = true;
            this.ButtonViewData.Click += new System.EventHandler(this.ButtonViewData_Click);
            // 
            // FormPhonemeCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 431);
            this.Controls.Add(this.ButtonViewData);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.ButtonInsert);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DDLCategory);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DDLLanguage);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormPhonemeCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phoneme Category Tool";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox DDLLanguage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TBASCII;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox DDLCategory;
        private System.Windows.Forms.Button ButtonInsert;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button ButtonViewData;
    }
}

