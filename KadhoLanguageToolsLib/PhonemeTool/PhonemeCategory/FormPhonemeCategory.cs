﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using KadhoLanguageLib;

namespace PhonemeTool
{
    public partial class FormPhonemeCategory : Form
    {
        private List<Phoneme> phonemes;
        private PhonemeCategoryLibrary pCatLib;

        public FormPhonemeCategory()
        {
            InitializeComponent();

            // setup language combo boxes
            for (LanguageType i = LanguageType.Mandarin; i < LanguageType.MaxType; i++)
                DDLLanguage.Items.Add(i.ToString());

            // setup phoneme category
            foreach (PhonemeCatogoryType i 
                in Enum.GetValues(typeof(PhonemeCatogoryType)).Cast<PhonemeCatogoryType>().ToList().OrderBy(a => a.ToString()))
                DDLCategory.Items.Add(i.ToString());

            //openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //openFileDialog1.FilterIndex = 1;
            //openFileDialog1.Title = "Select txt file of phoneme data";

            //DialogResult result = openFileDialog1.ShowDialog();

            //if (!result.ToString().Equals("OK"))
            //    return;

            //// load ipa list 
            //phonemes = IPAUtility.GetPhonemes(File.ReadAllText(openFileDialog1.FileName));

            // load previous ipa list if exists
            if (File.Exists(Globals.PhonemesFile))
                phonemes = IPAUtility.GetPhonemes(File.ReadAllText(Globals.PhonemesFile));
            else
                phonemes = new List<Phoneme>();

            pCatLib = new PhonemeCategoryLibrary();
            if (File.Exists(Globals.PhonemeCategoryFile))
                pCatLib.ImportData(IPAUtility.GetPhonemeCategoryData(File.ReadAllText(Globals.PhonemeCategoryFile)));
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            TBASCII.Text = string.Empty;
        }

        private void ButtonInsert_Click(object sender, EventArgs e)
        {
            if (DDLLanguage.SelectedItem == null || DDLCategory.SelectedItem == null)
                return;

            LanguageType language = (LanguageType)Enum.Parse(typeof(LanguageType), (string)DDLLanguage.SelectedItem);
            PhonemeCatogoryType category = (PhonemeCatogoryType)Enum.Parse(typeof(PhonemeCatogoryType), (string)DDLCategory.SelectedItem);

            string[] asciiStrings = Regex.Split(TBASCII.Text, "\r\n");

            foreach (string ascii in asciiStrings)
            {
                if (string.IsNullOrEmpty(ascii))
                    continue;

                Phoneme p = phonemes.FirstOrDefault(a => a.ASCII == ascii);
                if (p != null)
                    pCatLib.AddPhonemeCategory(language, p.Id, category);
                else
                    MessageBox.Show("Error. " + ascii + " is not a valid phoneme.");
            }

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PhonemeCategoryData[]));
            FileStream file = new FileStream(Globals.PhonemeCategoryFile, FileMode.OpenOrCreate);
            ser.WriteObject(file, pCatLib.GetExportData());
            file.Close();
        }

        private void ButtonViewData_Click(object sender, EventArgs e)
        {
            FormViewPhonemeCategoryData formViewData = new FormViewPhonemeCategoryData(phonemes, pCatLib);
            formViewData.Show();
        }
    }
}
