﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool
{
    public partial class FormViewPhonemeCategoryData : Form
    {
        private List<Phoneme> phonemes;
        private PhonemeCategoryLibrary pCatLib;

        public FormViewPhonemeCategoryData(List<Phoneme> phonemes, PhonemeCategoryLibrary pCatLib)
        {
            InitializeComponent();
            this.phonemes = phonemes;
            this.pCatLib = pCatLib;

            // setup language combo boxes
            for (LanguageType i = LanguageType.Mandarin; i < LanguageType.MaxType; i++)
                DDLLanguage.Items.Add(i.ToString());

        }

        private void DDLLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            LanguageType language = (LanguageType)Enum.Parse(typeof(LanguageType), (string)DDLLanguage.SelectedItem);

            DGVData.Rows.Clear();

            Dictionary<int, PhonemeCatogoryType> phonemeDictionary = pCatLib.GetPhonemeCategoryDictionary(language);
            if (phonemeDictionary == null)
                return;

            foreach (KeyValuePair<int, PhonemeCatogoryType> kvp in phonemeDictionary)
            {
                Phoneme p = phonemes.First(a => a.Id == kvp.Key);
                PhonemeCatogoryType[] types = PhonemeCategoryLibrary.GetPhonemeCategoryArray(kvp.Value);
                DGVData.Rows.Add(new object[] { p.Id, p.IPA, p.ASCII, string.Join(", ", types) });
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LanguageType language = (LanguageType)Enum.Parse(typeof(LanguageType), (string)DDLLanguage.SelectedItem);

            Dictionary<int, PhonemeCatogoryType> phonemeDictionary = pCatLib.GetPhonemeCategoryDictionary(language);
            if (phonemeDictionary == null)
                return;

            saveFileDialog1.Title = "Export Current Language Phoneme Category Data";
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";

            DialogResult result = saveFileDialog1.ShowDialog();

            if (!result.ToString().Equals("OK"))
                return;

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PhonemeCategoryData[]));
            FileStream file = new FileStream(saveFileDialog1.FileName, FileMode.OpenOrCreate);
            ser.WriteObject(file, pCatLib.GetExportData(language));
            file.Close();
        }
    }
}
