﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool.Words
{
    public partial class FormViewWords : Form
    {
        private List<WordObject> words;

        public FormViewWords(List<WordObject> words)
        {
            InitializeComponent();
            this.words = words;

            foreach (WordObject w in words)
            {
                DGVWords.Rows.Add(
                    new object[] 
                    { 
                        w.Word,
                        w.EnglishWord,
                        string.Join(" ", w.Syllables),
                        string.Join(" ' ", w.PhonemeSyllables.Select(a => string.Join(" ", a.Select(b => b.IPA)))),
                        string.Join(" ' ", w.PhonemeSyllables.Select(a => string.Join(" ", a.Select(b => b.ASCII)))),
                        string.Join(" ", w.SylTypes)
                    });
            }
        }


    }
}
