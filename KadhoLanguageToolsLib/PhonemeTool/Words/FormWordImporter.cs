﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool.Words
{
    public partial class FormWordImporter : Form
    {
        private List<Phoneme> phonemes;
        private List<WordObject> words;

        public FormWordImporter()
        {
            InitializeComponent();
            words = new List<WordObject>();

            // load previous phoneme list if exists
            if (File.Exists(Globals.PhonemesFile))
                phonemes = IPAUtility.GetPhonemes(File.ReadAllText(Globals.PhonemesFile));
            else
                phonemes = new List<Phoneme>();
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            TBEnglishWord.Text = string.Empty;
            TBIPASyllables.Text = string.Empty;
            TBSyllables.Text = string.Empty;
            TBSylType.Text = string.Empty;
            TBWord.Text = string.Empty;
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (TBEnglishWord.Text.Length == 0)
                TBEnglishWord.Text = TBWord.Text;

            string[] rawWords = Regex.Split(TBWord.Text, "\r\n").Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim()).ToArray();
            string[] englishWords = Regex.Split(TBEnglishWord.Text, "\r\n").Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim()).ToArray();
            string[] syllables = Regex.Split(TBSyllables.Text, "\r\n").Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim()).ToArray();
            string[] ipaSyllables = Regex.Split(TBIPASyllables.Text, "\r\n").Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim()).ToArray();
            string[] sylTypes = Regex.Split(TBSylType.Text, "\r\n").Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim()).ToArray();

            if (rawWords.Length == 0)
                return;
            if (rawWords.Length != englishWords.Length || rawWords.Length != syllables.Length || rawWords.Length != ipaSyllables.Length
                || rawWords.Length != sylTypes.Length)
            {
                MessageBox.Show("Error. Inconsistent data lengths.");
                return;
            }

            for (int i = 0; i < rawWords.Length; i++)
            {
                // update
                if (words.Exists(a => a.Word == rawWords[i]))
                {
                    WordObject w = words.First(a => a.Word == rawWords[i]);
                    w.Word = rawWords[i];
                    w.EnglishWord = englishWords[i];
                    w.Syllables = WordObject.GetSyllables(syllables[i]);
                    w.PhonemeSyllables = WordObject.GetPhonemes(ipaSyllables[i], phonemes);
                    w.SylTypes = WordObject.GetSylTypes(sylTypes[i]);
                }
                else
                {
                    WordObject w = new WordObject();
                    w.Word = rawWords[i];
                    w.EnglishWord = englishWords[i];
                    w.Syllables = WordObject.GetSyllables(syllables[i]);
                    w.PhonemeSyllables = WordObject.GetPhonemes(ipaSyllables[i], phonemes);
                    w.SylTypes = WordObject.GetSylTypes(sylTypes[i]);
                    words.Add(w);
                }
            }
        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            words.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Title = "Select txt file of words";

            DialogResult result = openFileDialog1.ShowDialog();

            if (!result.ToString().Equals("OK"))
                return;

            words = IPAUtility.GetWords(File.ReadAllText(openFileDialog1.FileName), phonemes);
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.Title = "Export Words";

            DialogResult result = saveFileDialog1.ShowDialog();

            if (!result.ToString().Equals("OK"))
                return;
            
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(WordObjectData[]));
            FileStream file = new FileStream(saveFileDialog1.FileName, FileMode.OpenOrCreate);
            ser.WriteObject(file, words.Select(a => a.GetExportData()).ToArray());
            file.Close();
        }

        private void ButtonViewData_Click(object sender, EventArgs e)
        {
            FormViewWords form = new FormViewWords(words);
            form.Show();
        }
    }
}
