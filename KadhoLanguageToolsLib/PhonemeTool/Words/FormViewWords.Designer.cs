﻿namespace PhonemeTool.Words
{
    partial class FormViewWords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVWords = new System.Windows.Forms.DataGridView();
            this.Word = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnglishWord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Syllables = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IPA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ASCII = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SylTypes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGVWords)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVWords
            // 
            this.DGVWords.AllowUserToAddRows = false;
            this.DGVWords.AllowUserToDeleteRows = false;
            this.DGVWords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVWords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVWords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Word,
            this.EnglishWord,
            this.Syllables,
            this.IPA,
            this.ASCII,
            this.SylTypes});
            this.DGVWords.Location = new System.Drawing.Point(12, 12);
            this.DGVWords.Name = "DGVWords";
            this.DGVWords.ReadOnly = true;
            this.DGVWords.Size = new System.Drawing.Size(635, 537);
            this.DGVWords.TabIndex = 0;
            // 
            // Word
            // 
            this.Word.HeaderText = "Word";
            this.Word.Name = "Word";
            this.Word.ReadOnly = true;
            // 
            // EnglishWord
            // 
            this.EnglishWord.HeaderText = "English Word";
            this.EnglishWord.Name = "EnglishWord";
            this.EnglishWord.ReadOnly = true;
            // 
            // Syllables
            // 
            this.Syllables.HeaderText = "Syllables";
            this.Syllables.Name = "Syllables";
            this.Syllables.ReadOnly = true;
            // 
            // IPA
            // 
            this.IPA.HeaderText = "IPA";
            this.IPA.Name = "IPA";
            this.IPA.ReadOnly = true;
            // 
            // ASCII
            // 
            this.ASCII.HeaderText = "ASCII";
            this.ASCII.Name = "ASCII";
            this.ASCII.ReadOnly = true;
            // 
            // SylTypes
            // 
            this.SylTypes.HeaderText = "Syl Types";
            this.SylTypes.Name = "SylTypes";
            this.SylTypes.ReadOnly = true;
            // 
            // FormViewWords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 561);
            this.Controls.Add(this.DGVWords);
            this.Name = "FormViewWords";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Words";
            ((System.ComponentModel.ISupportInitialize)(this.DGVWords)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVWords;
        private System.Windows.Forms.DataGridViewTextBoxColumn Word;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnglishWord;
        private System.Windows.Forms.DataGridViewTextBoxColumn Syllables;
        private System.Windows.Forms.DataGridViewTextBoxColumn IPA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ASCII;
        private System.Windows.Forms.DataGridViewTextBoxColumn SylTypes;
    }
}