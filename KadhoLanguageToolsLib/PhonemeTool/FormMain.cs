﻿using PhonemeTool.Words;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void ButtonPhonemeCategory_Click(object sender, EventArgs e)
        {
            FormPhonemeCategory form = new FormPhonemeCategory();
            form.Show();
        }

        private void ButtonIPAASCII_Click(object sender, EventArgs e)
        {
            FormIPAASCIIImporter form = new FormIPAASCIIImporter();
            form.Show();
        }

        private void ButtonSylTypes_Click(object sender, EventArgs e)
        {
            FormSylType form = new FormSylType();
            form.Show();
        }

        private void ButtonWordsImporter_Click(object sender, EventArgs e)
        {
            FormWordImporter form = new FormWordImporter();
            form.Show();
        }
    }
}
