﻿namespace PhonemeTool
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonIPAASCII = new System.Windows.Forms.Button();
            this.ButtonPhonemeCategory = new System.Windows.Forms.Button();
            this.ButtonSylTypes = new System.Windows.Forms.Button();
            this.ButtonWordsImporter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonIPAASCII
            // 
            this.ButtonIPAASCII.Location = new System.Drawing.Point(70, 23);
            this.ButtonIPAASCII.Name = "ButtonIPAASCII";
            this.ButtonIPAASCII.Size = new System.Drawing.Size(139, 35);
            this.ButtonIPAASCII.TabIndex = 0;
            this.ButtonIPAASCII.Text = "Phoneme IPA + ASCII";
            this.ButtonIPAASCII.UseVisualStyleBackColor = true;
            this.ButtonIPAASCII.Click += new System.EventHandler(this.ButtonIPAASCII_Click);
            // 
            // ButtonPhonemeCategory
            // 
            this.ButtonPhonemeCategory.Location = new System.Drawing.Point(70, 64);
            this.ButtonPhonemeCategory.Name = "ButtonPhonemeCategory";
            this.ButtonPhonemeCategory.Size = new System.Drawing.Size(139, 35);
            this.ButtonPhonemeCategory.TabIndex = 1;
            this.ButtonPhonemeCategory.Text = "Phoneme Categories";
            this.ButtonPhonemeCategory.UseVisualStyleBackColor = true;
            this.ButtonPhonemeCategory.Click += new System.EventHandler(this.ButtonPhonemeCategory_Click);
            // 
            // ButtonSylTypes
            // 
            this.ButtonSylTypes.Location = new System.Drawing.Point(70, 105);
            this.ButtonSylTypes.Name = "ButtonSylTypes";
            this.ButtonSylTypes.Size = new System.Drawing.Size(139, 35);
            this.ButtonSylTypes.TabIndex = 2;
            this.ButtonSylTypes.Text = "Syl Types";
            this.ButtonSylTypes.UseVisualStyleBackColor = true;
            this.ButtonSylTypes.Click += new System.EventHandler(this.ButtonSylTypes_Click);
            // 
            // ButtonWordsImporter
            // 
            this.ButtonWordsImporter.Location = new System.Drawing.Point(70, 146);
            this.ButtonWordsImporter.Name = "ButtonWordsImporter";
            this.ButtonWordsImporter.Size = new System.Drawing.Size(139, 35);
            this.ButtonWordsImporter.TabIndex = 3;
            this.ButtonWordsImporter.Text = "Words Importer";
            this.ButtonWordsImporter.UseVisualStyleBackColor = true;
            this.ButtonWordsImporter.Click += new System.EventHandler(this.ButtonWordsImporter_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 338);
            this.Controls.Add(this.ButtonWordsImporter);
            this.Controls.Add(this.ButtonSylTypes);
            this.Controls.Add(this.ButtonPhonemeCategory);
            this.Controls.Add(this.ButtonIPAASCII);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phoneme Tool";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonIPAASCII;
        private System.Windows.Forms.Button ButtonPhonemeCategory;
        private System.Windows.Forms.Button ButtonSylTypes;
        private System.Windows.Forms.Button ButtonWordsImporter;
    }
}