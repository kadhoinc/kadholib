﻿namespace PhonemeTool
{
    partial class FormIPAASCIIImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBIPA = new System.Windows.Forms.TextBox();
            this.TBASCII = new System.Windows.Forms.TextBox();
            this.ButtonProcess = new System.Windows.Forms.Button();
            this.ButtonViewData = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonRename = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBIPA
            // 
            this.TBIPA.Location = new System.Drawing.Point(6, 19);
            this.TBIPA.Multiline = true;
            this.TBIPA.Name = "TBIPA";
            this.TBIPA.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBIPA.Size = new System.Drawing.Size(204, 236);
            this.TBIPA.TabIndex = 0;
            // 
            // TBASCII
            // 
            this.TBASCII.Location = new System.Drawing.Point(6, 19);
            this.TBASCII.Multiline = true;
            this.TBASCII.Name = "TBASCII";
            this.TBASCII.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBASCII.Size = new System.Drawing.Size(204, 236);
            this.TBASCII.TabIndex = 1;
            // 
            // ButtonProcess
            // 
            this.ButtonProcess.Location = new System.Drawing.Point(455, 107);
            this.ButtonProcess.Name = "ButtonProcess";
            this.ButtonProcess.Size = new System.Drawing.Size(75, 23);
            this.ButtonProcess.TabIndex = 2;
            this.ButtonProcess.Text = "Process";
            this.ButtonProcess.UseVisualStyleBackColor = true;
            this.ButtonProcess.Click += new System.EventHandler(this.ButtonProcess_Click);
            // 
            // ButtonViewData
            // 
            this.ButtonViewData.Location = new System.Drawing.Point(455, 136);
            this.ButtonViewData.Name = "ButtonViewData";
            this.ButtonViewData.Size = new System.Drawing.Size(75, 23);
            this.ButtonViewData.TabIndex = 10;
            this.ButtonViewData.Text = "View Data";
            this.ButtonViewData.UseVisualStyleBackColor = true;
            this.ButtonViewData.Click += new System.EventHandler(this.ButtonViewData_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TBIPA);
            this.groupBox1.Location = new System.Drawing.Point(12, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 261);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IPA";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TBASCII);
            this.groupBox2.Location = new System.Drawing.Point(229, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 261);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ASCII";
            // 
            // ButtonRename
            // 
            this.ButtonRename.Location = new System.Drawing.Point(455, 227);
            this.ButtonRename.Name = "ButtonRename";
            this.ButtonRename.Size = new System.Drawing.Size(75, 42);
            this.ButtonRename.TabIndex = 13;
            this.ButtonRename.Text = "Rename Sounds";
            this.ButtonRename.UseVisualStyleBackColor = true;
            this.ButtonRename.Click += new System.EventHandler(this.ButtonRename_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // FormIPAASCIIImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 295);
            this.Controls.Add(this.ButtonRename);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonViewData);
            this.Controls.Add(this.ButtonProcess);
            this.Name = "FormIPAASCIIImporter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IPA ASCII Importer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TBIPA;
        private System.Windows.Forms.TextBox TBASCII;
        private System.Windows.Forms.Button ButtonProcess;
        private System.Windows.Forms.Button ButtonViewData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button ButtonRename;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

