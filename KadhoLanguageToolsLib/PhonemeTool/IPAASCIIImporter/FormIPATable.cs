﻿using KadhoLanguageLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhonemeTool
{
    public partial class FormIPATable : Form
    {
        private List<Phoneme> ipaList;
        public FormIPATable(List<Phoneme> ipaList)
        {
            InitializeComponent();

            this.ipaList = ipaList;

            foreach (Phoneme i in ipaList)
            {
                DGVTable.Rows.Add(new object[] { i.Id, i.IPA, i.ASCII});
            }
            
        }
    }
}
