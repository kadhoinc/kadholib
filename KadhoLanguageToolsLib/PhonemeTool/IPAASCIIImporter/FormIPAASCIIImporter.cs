﻿using KadhoLanguageLib;
using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PhonemeTool
{
    public partial class FormIPAASCIIImporter : Form
    {
        private List<Phoneme> phonemes;

        public FormIPAASCIIImporter()
        {
            InitializeComponent();

            // load previous ipa list if exists
            if (File.Exists(Globals.PhonemesFile))
                phonemes = IPAUtility.GetPhonemes(File.ReadAllText(Globals.PhonemesFile));
            else
                phonemes = new List<Phoneme>();
        }

        private void ButtonProcess_Click(object sender, EventArgs e)
        {
            string[] ipaStrings = Regex.Split(TBIPA.Text, "\r\n");
            string[] asciiStrings = Regex.Split(TBASCII.Text, "\r\n");

            if (ipaStrings.Length != asciiStrings.Length)
            {
                MessageBox.Show("Error. IPA and ASCII list do not match in length.");
                return;
            }

            int id = 1;
            if (phonemes.Count > 0)
                id = phonemes.Select(ipa => ipa.Id).Max() + 1;

            for (int i = 0; i < ipaStrings.Length; i++)
            {
                if (string.IsNullOrEmpty(ipaStrings[i]) || string.IsNullOrEmpty(asciiStrings[i]))
                    continue;
                Phoneme ipaVal = new Phoneme()
                {
                    IPA = ipaStrings[i],
                    ASCII = asciiStrings[i],
                };

                if (!phonemes.Contains(ipaVal))
                {
                    ipaVal.Id = id++;
                    phonemes.Add(ipaVal);
                }
                else // update
                {
                    Phoneme ipaUpdate = phonemes.Find(a => a.IPA == ipaVal.IPA);
                    ipaUpdate.ASCII = ipaVal.ASCII;
                }
            }

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Phoneme[]));
            FileStream file = new FileStream(Globals.PhonemesFile, FileMode.OpenOrCreate);
            ser.WriteObject(file, phonemes.ToArray());
            file.Close();
        }

        private void ButtonViewData_Click(object sender, EventArgs e)
        {
            FormIPATable formTable = new FormIPATable(phonemes);
            formTable.ShowDialog();
        }

        private void ButtonRename_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "mp3 files (*.mp3)|*.mp3|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Title = "Select mp3 files to rename and duplicate";

            DialogResult result = openFileDialog1.ShowDialog();

            if (!result.ToString().Equals("OK"))
                return;

            List<string> missingFiles = new List<string>();
            foreach (string file in openFileDialog1.FileNames)
            {
                string renamedDirectory = Path.Combine(Path.GetDirectoryName(file), "Renamed");
                if (!Directory.Exists(renamedDirectory))
                    Directory.CreateDirectory(renamedDirectory);

                string fileName = Path.GetFileNameWithoutExtension(file);
                Phoneme ipaVal = phonemes.FirstOrDefault(i => i.ASCII == fileName);
                if (ipaVal != null)
                    File.Copy(file, Path.Combine(renamedDirectory, ipaVal.Id + Path.GetExtension(file)), true);
                else // missing
                    missingFiles.Add(Path.GetFileName(file));
            }

            if (missingFiles.Count > 0)
                MessageBox.Show("The following files do not have a matching ASCII value: \n" + string.Join("\n", missingFiles));

        }
    }
}
