﻿namespace PhonemeTool
{
    partial class FormIPATable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVTable = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IPA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ASCII = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGVTable)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVTable
            // 
            this.DGVTable.AllowUserToAddRows = false;
            this.DGVTable.AllowUserToDeleteRows = false;
            this.DGVTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.IPA,
            this.ASCII});
            this.DGVTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVTable.Location = new System.Drawing.Point(0, 0);
            this.DGVTable.Name = "DGVTable";
            this.DGVTable.ReadOnly = true;
            this.DGVTable.Size = new System.Drawing.Size(217, 484);
            this.DGVTable.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 50;
            // 
            // IPA
            // 
            this.IPA.HeaderText = "IPA";
            this.IPA.Name = "IPA";
            this.IPA.ReadOnly = true;
            this.IPA.Width = 50;
            // 
            // ASCII
            // 
            this.ASCII.HeaderText = "ASCII";
            this.ASCII.Name = "ASCII";
            this.ASCII.ReadOnly = true;
            this.ASCII.Width = 50;
            // 
            // FormIPATable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(217, 484);
            this.Controls.Add(this.DGVTable);
            this.Name = "FormIPATable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IPA Table";
            ((System.ComponentModel.ISupportInitialize)(this.DGVTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IPA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ASCII;

    }
}