﻿using System;

namespace PhonemeTool
{
    public class Globals
    {
        public const string PhonemesFile = "phonemes.txt";
        public const string PhonemeCategoryFile = "phoneme_categories.txt";
        public const string SylTypesFile = "syl_types.txt";
    }
}
