﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KadhoLib.Utility
{
    public class ExceptionUtility
    {
        /// <summary>
        /// Flattens the exception message by getting all messages from inner exceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="inclSource"></param>
        /// <param name="inclStacktrace"></param>
        /// <returns></returns>
        public static string FlattenException(Exception ex, bool inclSource = false, bool inclStacktrace = false)
        {
            StringBuilder sb = new StringBuilder();
            while (ex != null)
            {
                sb.AppendLine("Message: " + ex.Message);
                if (inclSource)
                    sb.AppendLine("Source: " + ex.Source);
                if (inclStacktrace)
                    sb.AppendLine("Stack Trace: " + ex.StackTrace);
                ex = ex.InnerException;
            }
            return sb.ToString();
        }
    }
}
