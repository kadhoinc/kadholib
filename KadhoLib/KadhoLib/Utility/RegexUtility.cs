﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
#if DEBUG
using System.Diagnostics;
#endif

namespace KadhoLib.Utility
{
    public class RegexUtility
    {
        #region Public static methods

        /// <summary>
        /// Check if the email is valid.
        /// Source modified from: http://msdn.microsoft.com/en-us/library/01escwtf(v=vs.110).aspx
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None);
            }
            catch (Exception ex)
            {
#if DEBUG
                // log error
                Debug.WriteLine(ex.Message);
#endif
                return false;
            }

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(email,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase);
            }
            catch (Exception ex)
            {
#if DEBUG
                // log error
                Debug.WriteLine(ex.Message);
#endif
                return false;
            }
        }

        /// <summary>
        /// Check if the username is valid according to Kadho standards.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool IsValidUsername(string username)
        {
            Regex strPattern = new Regex("^[0-9A-Za-z_@.]+$");
            return strPattern.IsMatch(username);
        }

        #endregion

        #region Private static helper methods

        /// <summary>
        /// Helper method for IsValidEmail.
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            domainName = idn.GetAscii(domainName);
            return match.Groups[1].Value + domainName;
        }

        #endregion

    }
}
