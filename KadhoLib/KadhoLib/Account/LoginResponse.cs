﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KadhoLib.Account
{
    public class LoginResponse
    {
        public enum ResponseType
        {
            None,
            Pending,
            Successful,
            Failed,
        }

        #region Properties

        /// <summary>
        /// Response type.
        /// </summary>
        public ResponseType Response { get; set; }

        /// <summary>
        /// Response exception if any.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Response message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Thread task set when logging in.
        /// </summary>
        public Task LoginTask { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns true if the sign up task thread has ran to completion.
        /// </summary>
        /// <returns></returns>
        public bool IsLoginCompleted()
        {
            if (LoginTask != null)
                return LoginTask.IsCompleted;
            return false;
        }

        /// <summary>
        /// Return true if the response is an error.
        /// </summary>
        /// <returns></returns>
        public bool IsLoginFailed()
        {
            return Response == ResponseType.Failed;
        }

        #endregion
    }
}
