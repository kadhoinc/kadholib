﻿#if DEBUG
using System.Diagnostics;
#endif

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using KadhoLib.Utility;
using Parse;
using System.Threading.Tasks;

namespace KadhoLib.Account
{
    public class AccountManager
    {
        #region Constants

        private const int minimumPasswordLength = 4;

        #endregion

        #region Public static methods

        /// <summary>
        /// Sign up the user with first name, last name, username, password, email, country, and state.
        /// Returns a SignUpResponse of the SignUpTask if successful, else check Response property.
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="confirmPassword"></param>
        /// <param name="email"></param>
        /// <param name="confirmEmail"></param>
        /// <param name="country"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static SignUpResponse SignUp(string firstName, string lastName, 
            string username, string password, string confirmPassword,
            string email, string confirmEmail, string country, string state)
        {
            // remove whitespaces all fields
            if (!string.IsNullOrWhiteSpace(username))
                username = username.Trim();
            if (!string.IsNullOrWhiteSpace(password))
                password = password.Trim();
            if (!string.IsNullOrWhiteSpace(confirmPassword))
                confirmPassword = confirmPassword.Trim();
            if (!string.IsNullOrWhiteSpace(email))
                email = email.Trim().ToLower();
            if (!string.IsNullOrWhiteSpace(confirmEmail))
                confirmEmail = confirmEmail.Trim().ToLower();
            if (!string.IsNullOrWhiteSpace(country))
                country = country.Trim();
            if (!string.IsNullOrWhiteSpace(state))
                state = state.Trim();

            // check if valid username
            if (!RegexUtility.IsValidUsername(username))
            {
                string errorMsg = "Unacceptable characters in username. Username must only contain letters and digits.";
#if DEBUG
                // log error
                Debug.WriteLine(errorMsg + " Username: " + username);
#endif
                return new SignUpResponse()
                {
                    Message = errorMsg,
                    Response = SignUpResponse.ResponseType.UsernameError
                };
            }

            // check if email matches
            if (email != confirmEmail)
            {
                string errorMsg = "Email does not match confirmation email.";
#if DEBUG
                // log error
                Debug.WriteLine(errorMsg + "\nEmail: " + email + "\nConfirm Email: " + confirmEmail);
#endif
                return new SignUpResponse()
                {
                    Message = errorMsg,
                    Response = SignUpResponse.ResponseType.EmailError
                };
            }

            // check if email is valid
            if (!RegexUtility.IsValidEmail(email))
            {
                string errorMsg = "Invalid email.";
#if DEBUG
                // log error
                Debug.WriteLine(errorMsg + "\nEmail: " + email);
#endif

                return new SignUpResponse()
                {
                    Message = errorMsg,
                    Response = SignUpResponse.ResponseType.EmailError
                };
            }

            // check if password matches confirm password
            if (password != confirmPassword)
            {
                string errorMsg = "Password does not match confirmation password.";
#if DEBUG
                // log error
                Debug.WriteLine(errorMsg + "\nPassword: " + password + "\nConfirmPassword: " + confirmPassword);
#endif
                return new SignUpResponse()
                {
                    Message = errorMsg,
                    Response = SignUpResponse.ResponseType.PasswordError
                };
            }

            // check if password is valid
            if (password.Length < minimumPasswordLength)
            {
                string errorMsg = "Password must be at least " + minimumPasswordLength + " characters long.";
#if DEBUG
                // log error
                Debug.WriteLine(errorMsg + "\nPassword: " + password);
#endif
                return new SignUpResponse()
                {
                    Message = errorMsg,
                    Response = SignUpResponse.ResponseType.PasswordError
                };
            }

            KadhoUser user = new KadhoUser()
            {
                FirstName = firstName,
                LastName = lastName,
                Username = username,
                Password = password,
                Email = email,
                Country = country,
                State = state,
            };

            SignUpResponse r = new SignUpResponse()
            {
                Message = "Sign up pending.",
                Response = SignUpResponse.ResponseType.Pending,
                User = user
            };

            try
            {
                // sign up user
                Task signUpTask = user.UserObject.SignUpAsync().ContinueWith(t =>
                    {
                        if (t.IsFaulted || t.IsCanceled)
                        {
                            r.Response = SignUpResponse.ResponseType.GenericError;
                            r.Exception = t.Exception;
#if DEBUG
                            Debug.WriteLine(ExceptionUtility.FlattenException(t.Exception));
#endif

                            // Errors from Parse Cloud and network interactions
                            using (IEnumerator<Exception> enumerator = t.Exception.InnerExceptions.GetEnumerator())
                            {
                                if (enumerator.MoveNext())
                                {
                                    ParseException error = (ParseException)enumerator.Current;
#if DEBUG
                                    Debug.WriteLine(error.Message.ToString() + " code:" + (int)error.Code);
#endif

                                    // update the SignUpResponse with the error
                                    r.Message = error.Message.ToString();
                                }
                            }
                        }
                        else // success
                        {
                            // update the SignUpResponse when async task is complete
                            r.Message = "Sign up successful.";
                            r.Response = SignUpResponse.ResponseType.Successful;
#if DEBUG
                            Debug.WriteLine("User created. Username: " + username + " Password : " + password + " e-mail: " + email);
#endif
                        }
                    });

                r.SignUpTask = signUpTask;
            }
            catch (Exception ex)
            {
                r.Message = ex.Message;
                r.Exception = ex;
                r.Response = SignUpResponse.ResponseType.GenericError;
#if DEBUG
                Debug.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
            }

            return r;
        }

        /// <summary>
        /// Login with the username and password.
        /// Returns the LoginResponse.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static LoginResponse Login(string username, string password)
        {
            // remove trailing white spaces
            username = username.Trim();
            password = password.Trim();

            // check for empty username
            if (string.IsNullOrEmpty(username))
            {
#if DEBUG
                Debug.WriteLine("Empty username.");
#endif
                return new LoginResponse()
                {
                    Response = LoginResponse.ResponseType.Failed,
                    Message = "Empty username.",
                };
            }

            // check for empty password
            if (string.IsNullOrEmpty(password))
            {
#if DEBUG
                Debug.WriteLine("Empty password.");
#endif
                return new LoginResponse()
                {
                    Response = LoginResponse.ResponseType.Failed,
                    Message = "Empty password.",
                };
            }

            LoginResponse r = new LoginResponse()
            {
                Response = LoginResponse.ResponseType.Pending
            };

            try
            {                
                System.Threading.Tasks.Task loginUser = ParseUser.LogInAsync(username, password).ContinueWith(t =>
                {
                    if (t.IsFaulted || t.IsCanceled)
                    {
                        r.Response = LoginResponse.ResponseType.Failed;
#if DEBUG
                        Debug.WriteLine(ExceptionUtility.FlattenException(t.Exception));
#endif

                        using (IEnumerator<System.Exception> enumerator = t.Exception.InnerExceptions.GetEnumerator())
                        {
                            if (enumerator.MoveNext())
                            {
                                ParseException error = (ParseException)enumerator.Current;
#if DEBUG
                                Debug.WriteLine(error.Message + " " + (int)error.Code + ": " + error.Code);
#endif
                                r.Message = error.Message;
                            }
                        }
                    }
                    else
                    {
                        //Login was successful
                        r.Message = "Login successful.";
#if DEBUG
                        Debug.WriteLine("User login successful. Username: " + username + " Password : " + password);
#endif
                    }
                });

                r.LoginTask = loginUser;
            }
            catch (Exception ex)
            {
                r.Message = ex.Message;
                r.Exception = ex;
                r.Response = LoginResponse.ResponseType.Failed;
#if DEBUG
                Debug.WriteLine(ExceptionUtility.FlattenException(ex));
#endif
            }

            return r;
        }
        
        #endregion
    }
}
