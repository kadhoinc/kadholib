﻿using System;
using System.Collections.Generic;
using Parse;

namespace KadhoLib.Account
{
    /// <summary>
    /// Wrapper object for a ParseUser. Provides methods of accessing extra data stored in a user.
    /// </summary>
    public class KadhoUser
    {
        #region Properties

        /// <summary>
        /// ParseUser object.
        /// </summary>
        public ParseUser UserObject { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username
        {
            get { return UserObject.Username; }
            set { UserObject.Username = value; }
        }

        /// <summary>
        /// Sets the password.
        /// </summary>
        public string Password
        {
            set { UserObject.Password = value; }
        }

        /// <summary>
        /// User email.
        /// </summary>
        public string Email
        {
            get { return UserObject.Email; }
            set { UserObject.Email = value; }
        }

        /// <summary>
        /// User first name.
        /// </summary>
        public string FirstName
        {
            get { return UserObject.Get<string>("firstName"); }
            set 
            {
                AddValue("firstName", value);
            }
        }

        /// <summary>
        /// User last name.
        /// </summary>
        public string LastName
        {
            get { return UserObject.Get<string>("lastName"); }
            set { AddValue("lastName", value); }
        }
        
        /// <summary>
        /// Get the full user first and last name.
        /// </summary>
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        /// <summary>
        /// User country.
        /// </summary>
        public string Country
        {
            get { return UserObject.Get<string>("country"); }
            set 
            {
                AddValue("country", value);
            }
        }

        /// <summary>
        /// User state.
        /// </summary>
        public string State
        {
            get { return UserObject.Get<string>("state"); }
            set { AddValue("state", value); }
        }

        /// <summary>
        /// Location of user in format of Country, State.
        /// </summary>
        public string Location
        {
            get { return Country + ", " + State; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Create a KadhoUser with no data.
        /// </summary>
        public KadhoUser()
        {
            UserObject = new ParseUser();
        }

        /// <summary>
        /// Create a KadhoUser using the ParseUser.
        /// </summary>
        /// <param name="user"></param>
        public KadhoUser(ParseUser user)
        {
            UserObject = user;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add or update the User with the key and value.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddValue(string key, object value)
        {
            if (UserObject.ContainsKey(key))
                UserObject[key] = value;
            else
                UserObject.Add(key, value);
        }

        #endregion
    }
}
