﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KadhoLib.Account
{
    public class SignUpResponse
    {
        public enum ResponseType
        {
            None,
            Pending,
            Successful,
            UsernameError,
            EmailError,
            PasswordError,
            GenericError,
        }

        #region Properties

        /// <summary>
        /// Response type.
        /// </summary>
        public ResponseType Response { get; set; }

        /// <summary>
        /// Response exception if any.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Response message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Thread task set when signing up.
        /// </summary>
        public Task SignUpTask { get; set; }

        /// <summary>
        /// User created when signing up.
        /// </summary>
        public KadhoUser User { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns true if the sign up task thread has ran to completion.
        /// </summary>
        /// <returns></returns>
        public bool IsSignUpCompleted()
        {
            if (SignUpTask != null)
                return SignUpTask.IsCompleted;
            return false;
        }

        /// <summary>
        /// Return true if the response is an error.
        /// </summary>
        /// <returns></returns>
        public bool IsError()
        {
            return Response != ResponseType.None && Response != ResponseType.Pending
                && Response != ResponseType.Successful;
        }

        #endregion
    }
}
