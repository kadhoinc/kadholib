﻿using System;
using System.Collections.Generic;
using Parse;

namespace KadhoLib.Parse
{
    /// <summary>
    /// ParseObject wrapper with PlayerId and permissions set to the player.
    /// </summary>
    public class PlayerParseObject : ParseObjectWrapper
    {
        #region Properties

        /// <summary>
        /// Player username
        /// </summary>
        public string PlayerName
        {
            get { return ParseObject.Get<string>("playerName"); }
            set { AddValue("playerName", value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Create a new ParseObjectWrapper with a new ParseObject.
        /// </summary>
        public PlayerParseObject(string className)
            : base(className)
        {
            SetCurrentPlayer();
        }

        /// <summary>
        /// Create a ParseObjectWrapper with the parse object
        /// </summary>
        /// <param name="p"></param>
        public PlayerParseObject(ParseObject p)
            : base(p)
        {
            SetCurrentPlayer();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Set the current user ObjectId as the PlayerId and set ACL owner to user.
        /// </summary>
        public void SetCurrentPlayer()
        {
            if (ParseUser.CurrentUser != null)
                PlayerName = ParseUser.CurrentUser.Username;
            ParseObject.ACL = new ParseACL(ParseUser.CurrentUser);
        }

        #endregion
    }
}
