﻿using System;
using System.Collections.Generic;
using Parse;
using System.Linq;
using KadhoLib.Utility;

namespace KadhoLib.Parse
{
    /// <summary>
    /// ParseObjectWrapper provides convenient wrapping and helper methods for ParseObjects.
    /// </summary>
    public class ParseObjectWrapper
    {
        #region Properties

        /// <summary>
        /// Parse object
        /// </summary>
        public ParseObject ParseObject { get; private set; }            

        #endregion

        #region Constructor

        /// <summary>
        /// Create a new ParseObjectWrapper with a new ParseObject.
        /// </summary>
        public ParseObjectWrapper(string className)
        {
            ParseObject = new ParseObject(className);
        }

        /// <summary>
        /// Create a ParseObjectWrapper with the parse object
        /// </summary>
        /// <param name="p"></param>
        public ParseObjectWrapper(ParseObject p)
        {
            ParseObject = p;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add or update the ParseObject with the key and value.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddValue(string key, object value)
        {
            if (ParseObject.ContainsKey(key))
                ParseObject[key] = value;
            else
                ParseObject.Add(key, value);
        }

        /// <summary>
        /// Get the decimal array. Returns null if key does not exist or not convertable to a decimal array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public decimal[] GetDecimalArray(string key)
        {
            var o = GetDecimalEnumerable(key);
            if (o != null)
            {
                if (o is decimal[])
                    return (decimal[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the datetime array. Returns null if key does not exist or not convertable to a datetime array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public DateTime[] GetDateTimeArray(string key)
        {
            var o = GetDateTimeEnumerable(key);
            if (o != null)
            {
                if (o is DateTime[])
                    return (DateTime[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the double array. Returns null if key does not exist or not convertable to a double array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public double[] GetDoubleArray(string key)
        {
            var o = GetDoubleEnumerable(key);
            if (o != null)
            {
                if (o is double[])
                    return (double[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the float array. Returns null if key does not exist or not convertable to a float array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public float[] GetFloatArray(string key)
        {
            var o = GetFloatEnumerable(key);
            if (o != null)
            {
                if (o is float[])
                    return (float[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the string array. Returns null if key does not exist or not convertable to a string array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string[] GetStringArray(string key)
        {
            var o = GetStringEnumerable(key);
            if (o != null)
            {
                if (o is string[])
                    return (string[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the int32 array. Returns null if key does not exist or not convertable to a int array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int[] GetInt32Array(string key)
        {
            var o = GetInt32Enumerable(key);
            if (o != null)
            {
                if (o is int[])
                    return (int[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the bool array. Returns null if key does not exist or not convertable to a bool array.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool[] GetBoolArray(string key)
        {
            var o = GetBoolEnumerable(key);
            if (o != null)
            {
                if (o is bool[])
                    return (bool[])o;

                var oArray = o.ToArray();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }
        
        /// <summary>
        /// Get the decimal list. Returns null if key does not exist or not convertable to a decimal list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<decimal> GetDecimalList(string key)
        {
            var o = GetDecimalEnumerable(key);
            if (o != null)
            {
                if (o is List<decimal>)
                    return (List<decimal>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the datetime list. Returns null if key does not exist or not convertable to a datetime list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<DateTime> GetDateTimeList(string key)
        {
            var o = GetDateTimeEnumerable(key);
            if (o != null)
            {
                if (o is List<DateTime>)
                    return (List<DateTime>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the double list. Returns null if key does not exist or not convertable to a double list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<double> GetDoubleList(string key)
        {
            var o = GetDoubleEnumerable(key);
            if (o != null)
            {
                if (o is List<double>)
                    return (List<double>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the float list. Returns null if key does not exist or not convertable to a float list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<float> GetFloatList(string key)
        {
            var o = GetFloatEnumerable(key);
            if (o != null)
            {
                if (o is List<float>)
                    return (List<float>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the string list. Returns null if key does not exist or not convertable to a string list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<string> GetStringList(string key)
        {
            var o = GetStringEnumerable(key);
            if (o != null)
            {
                if (o is List<string>)
                    return (List<string>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the int list. Returns null if key does not exist or not convertable to a int list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<int> GetInt32List(string key)
        {
            var o = GetInt32List(key);
            if (o != null)
            {
                if (o is List<int>)
                    return (List<int>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the bool list. Returns null if key does not exist or not convertable to a bool list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<bool> GetBoolList(string key)
        {
            var o = GetBoolEnumerable(key);
            if (o != null)
            {
                if (o is List<bool>)
                    return (List<bool>)o;

                var oArray = o.ToList();
                // reformat the data to get faster later
                AddValue(key, oArray);
                return oArray;
            }
            return null;
        }

        /// <summary>
        /// Get the decimal enumerable. Returns null if key does not exist or not convertable to a decimal enumerable
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<decimal> GetDecimalEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<DateTime>)
                return (IEnumerable<decimal>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToDecimal(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the datetime enumerable. Returns null if key does not exist or not convertable to a datetime enumerable
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<DateTime> GetDateTimeEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<DateTime>)
                return (IEnumerable<DateTime>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToDateTime(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the double enumerable. Returns null if key does not exist or not convertable to a double enumerable
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<double> GetDoubleEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<double>)
                return (IEnumerable<double>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToDouble(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the float enumerable. Returns null if key does not exist or not convertable to a float enumerable.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<float> GetFloatEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<float>)
                return (IEnumerable<float>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToSingle(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the int enumerable. Returns null if key does not exist or not convertable to a int enumerable.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<string> GetStringEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<string>)
                return (IEnumerable<string>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToString(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }
        
        /// <summary>
        /// Get the int enumerable. Returns null if key does not exist or not convertable to a int enumerable.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<int> GetInt32Enumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<int>)
                return (IEnumerable<int>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToInt32(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the bool enumerable. Returns null if key does not exist or not convertable to a bool enumerable.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<bool> GetBoolEnumerable(string key)
        {
            if (!ParseObject.ContainsKey(key))
                return null;

            object o = ParseObject.Get<object>(key);
            if (o is IEnumerable<bool>)
                return (IEnumerable<bool>)o;
            else
            {
                try
                {
                    // parse the object array
                    return ((List<object>)o).Select(a => Convert.ToBoolean(a));
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ExceptionUtility.FlattenException(ex, true, true));
#endif
                    return null;
                }
            }
        }

        #endregion
    }
}
