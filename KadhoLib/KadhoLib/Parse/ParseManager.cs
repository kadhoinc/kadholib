﻿using System.Collections;
using Parse;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

#if !UNITY_WEBPLAYER
using System.Net.NetworkInformation;
#endif

using System.Net;
using KadhoLib.Account;


namespace KadhoLib.Parse
{
    public class ParseManager
    {
        public enum StateType
        {
            Ready,
            SignUp,
            Login,
        }

        #region Properties

        /// <summary>
        /// Current state
        /// </summary>
        public StateType State { get; private set; }

        #endregion

        #region Event delegates

        public delegate void SignUpEvent(SignUpResponse r);
        public delegate void LoginEvent(LoginResponse r);
        public delegate void ParseManagerEvent(ParseManager p);

        #endregion

        #region Public events

        /// <summary>
        /// Event when sign up task completes.
        /// </summary>
        public event SignUpEvent OnSignUpFinish;

        /// <summary>
        /// Event when login task completes.
        /// </summary>
        public event LoginEvent OnLoginFinish;
        
        /// <summary>
        /// Event when manager logoff is called.
        /// </summary>
        public event ParseManagerEvent OnLogout;

        #endregion

        #region Fields

        private SignUpResponse signUpResponse;
        private LoginResponse loginResponse;

        #endregion

        #region Constructor

        public ParseManager()
        {
            State = StateType.Ready;
        }

        #endregion

        #region Unity methods

        // Use this for initialization
        public void Start()
        {
            State = StateType.Ready;
        }

        // Update is called once per frame
        public void Update()
        {
            switch (State)
            {
                case StateType.Login:
                    {
                        // check if login complete
                        if (loginResponse != null)
                        {
                            if (loginResponse.IsLoginCompleted() || loginResponse.IsLoginFailed())
                            {
                                // execute OnLoginFinish event
                                if (OnLoginFinish != null)
                                {
                                    OnLoginFinish(loginResponse);
                                    OnLoginFinish = null;
                                }
                                State = StateType.Ready;
                            }
                        }
                        else // error?
                            State = StateType.Ready;
                        break;
                    }
                case StateType.SignUp:
                    {
                        // check if sign up is complete or errored
                        if (signUpResponse != null)
                        {
                            if (signUpResponse.IsSignUpCompleted() || signUpResponse.IsError())
                            {
                                // execute OnSignUpFinish event
                                if (OnSignUpFinish != null)
                                {
                                    OnSignUpFinish(signUpResponse);
                                    OnSignUpFinish = null;
                                }
                                State = StateType.Ready;
                            }
                        }
                        else // error?
                            State = StateType.Ready;
                        break;
                    }
                default:
                    break;
            }
        }

//        void OnApplicationQuit()
//        {
//            if (ParseUser.CurrentUser != null)
//                ParseUser.LogOut();
//            var currentUser = ParseUser.CurrentUser;
//            if (currentUser == null)
//            {
//#if DEBUG
//                Debug.Log("Logged out successfully");
//#endif
//            }
//        }


        #endregion

        #region Public virtual methods

        //public virtual void CreateClass(string cName, string cKey, string cVal)
        //{
        //    Debug.Log("Clicked Create a class");
        //    if (cName == string.Empty || cKey == string.Empty || cVal == string.Empty)
        //    {
        //        Debug.LogWarning("One or more inputs are empty, info not sent to Parse");
        //        RecordError("Internal error", "You may not have empty fields in create class");
        //        return;
        //    }
        //    cName = NGUIText.StripSymbols(cName);
        //    cKey = NGUIText.StripSymbols(cKey);
        //    cVal = NGUIText.StripSymbols(cVal);

        //    ParseObject parseObject = new ParseObject(cName);
        //    parseObject[cKey] = cVal;
        //    parseObject["PlayerName"] = ParseUser.CurrentUser.Username;
        //    parseObject.SaveAsync();
        //    Debug.Log("Info sent to server.");
        //    Debug.Log("Class: " + cName + " Key: " + cKey + " Value: " + cVal);

        //}

        public virtual void CreateUser(string username, string password, string email)
        {
            signUpResponse = AccountManager.SignUp(string.Empty, string.Empty,
                username, password, password, email, email, string.Empty, string.Empty);
            State = StateType.SignUp;
        }

        public void LogOut()
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine(ParseUser.CurrentUser + " is logging out");
#endif
            ParseUser.LogOut();
            if (OnLogout != null)
            {
                OnLogout(this);
                OnLogout = null;
            }
            
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Log out successful.");
#endif
        }

        public virtual void LogIn(string username, string password)
        {
            loginResponse = AccountManager.Login(username, password);
            State = StateType.Login;
        }

        //public void SaveHighScore(string LevelName, int score, int photosTaken)
        //{
        //    if (ParseUser.CurrentUser != null)
        //    {
        //        ParseObject gameScore = new ParseObject(LevelName);
        //        gameScore["cookies"] = score;
        //        gameScore["photosTaken"] = photosTaken;
        //        gameScore["playerName"] = ParseUser.CurrentUser.Username;
        //        gameScore.SaveAsync();
        //        //gameScore.AddToList("cookie", )
        //    }

        //}

        #endregion

    }

}
