﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using KadhoLib.Utility;
using KadhoService.Models.Entity;
using KadhoService.Models.Entity.EarlyAccess;
using KadhoService.Models.Service;

namespace KadhoService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [RoutePrefix("api/earlyaccess")]
    public class EarlyAccessController : ApiController
    {
        #region AES Encryption consts

        private const string aesPassPhrase = "rrwgb8gsak70hbog";
        private const string aesSaltValue = "ym3v9jl2q0zf56b2";
        private const string aesInitVector = "m0239arrpahgm1h7";

        #endregion

        /// <summary>
        /// Get the current KadhoEarlyAccessUser from session. Null if not found.
        /// @TODO: Currently disabled.
        /// </summary>
        /// <returns></returns>
        //private KadhoEarlyAccessUser GetUserFromSession()
        //{
        //    if (HttpContext.Current.Session["User"] != null)
        //        return (KadhoEarlyAccessUser)HttpContext.Current.Session["User"];
        //    return null;
        //}

        /// <summary>
        /// Save the user to the session.
        /// @TODO: Currently disabled
        /// </summary>
        /// <param name="u"></param>
        //private void SaveUserToSession(KadhoEarlyAccessUser u)
        //{
        //    HttpContext.Current.Session["User"] = u;
        //}

        [Route("SignupEarlyAccess")]
        [HttpPost]
        public async Task<JsonResult<object>> SignupEarlyAccess(SignupEntity o)
        {
            EarlyAccessService eaService = new EarlyAccessService();

            // trim inputs
            if (!string.IsNullOrWhiteSpace(o.Name))
                o.Name = o.Name.Trim();
            // lower case email
            if (!string.IsNullOrWhiteSpace(o.Email))
                o.Email = o.Email.Trim().ToLower();

            KadhoEarlyAccessUser u = new KadhoEarlyAccessUser()
            {
                Name = o.Name,
                Email = o.Email,
                Zip = GetZip(o.Zip),
                ChildrenAge = GetChildrenAge(o.ChildrenAge),
                Subscribed = true,
                Referrer = o.Referrer,
            };

            try
            {
                EarlyAccessService.SignupResponse response = await eaService.Signup(u);

                switch (response.SRType)
                {
                    case EarlyAccessService.SignupResponse.SignupResponseType.Success:
                        {
                            //SaveUserToSession(u);

                            // send email
                            SendSignupEmail(u.Email, u.UserId);

                            return Json<object>(new
                            {
                                success = true,
                                message = response.Message,
                                user = new
                                {
                                    name = u.Name,
                                    email = u.Email,
                                    positionInLine = u.WaitListPosition,
                                    referCode = u.UserId,
                                    subscribed = u.Subscribed
                                }
                            });
                        }
                    case EarlyAccessService.SignupResponse.SignupResponseType.FailEmailExist:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidEmail:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidName:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidZip:
                        {
                            return Json<object>(new
                            {
                                success = false,
                                message = response.SRType.ToString(),
                            });
                        }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                string errStr = ExceptionUtility.FlattenException(ex, true, true);
                Console.WriteLine(errStr);
                return Json<object>(new
                {
                    success = false,
                    message = "Error"
                });
#else                
                return Json<object>(new
                {
                    success = false,
                    message = "Error",
                });
#endif
            }

            // default error
            return Json<object>(new
            {
                success = false,
                message = "Error",
            });
        }

        [Route("ResendEmail")]
        [HttpPost]
        public async Task<JsonResult<object>> ResendEmail(EmailEntity o)
        {
            string email = o.Email;
            if (string.IsNullOrWhiteSpace(email))
            {
                return Json<object>(
                    new
                    {
                        message = "ResendEmailError"
                    });
            }

            EarlyAccessService eaService = new EarlyAccessService();

            // trim email
            email = email.Trim().ToLower();

            // query parse to see if email exists
            if (await eaService.CanResendEmail(email))
            {
                await eaService.UpdateEmailResentTime(email, DateTime.Now);
                string userId = await eaService.GetUserIdByEmail(email);

                // send email
                SendSignupEmail(email, userId);

                return Json<object>(
                    new
                    {
                        message = "ResendEmailSuccess"
                    });
            }

            return Json<object>(
                new
                {
                    message = "RecentlySentEmail"
                });
        }

        [Route("GetCurrentUser")]
        [HttpGet]
        public JsonResult<object> GetCurrentUser()
        {
            //KadhoEarlyAccessUser u = GetUserFromSession();
            //if (u != null)
            //{
            //    return Json<object>(
            //        new
            //        {
            //            name = u.Name,
            //            email = u.Email,
            //            positionInLine = u.WaitListPosition,
            //        });
            //}

            return Json<object>(new { fail = true });
        }

        [Route("GetUserViewPosition")]
        [HttpGet]
        public async Task<JsonResult<object>> GetUserViewPosition(string viewPosition)
        {
            try
            {
                // url decode
                //viewPosition = HttpUtility.UrlDecode(viewPosition);

                // padding
                viewPosition = viewPosition.PadRight(viewPosition.Length + (4 - viewPosition.Length % 4) % 4, '=');

                string unencryptedLink = EncryptionUtility.AESDecrypt(viewPosition, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
                // split at : 
                // Format email:userid
                string[] split = unencryptedLink.Split(':');
                EarlyAccessService eaService = new EarlyAccessService();
                KadhoEarlyAccessUser u = await eaService.GetUser(split[0], split[1]);

                if (u != null)
                {
                    //SaveUserToSession(u);

                    return Json<object>(new
                    {
                        success = true,
                        user = new
                        {
                            name = u.Name,
                            email = u.Email,
                            positionInLine = u.WaitListPosition,
                            referCode = u.UserId,
                            subscribed = u.Subscribed
                        }
                    });
                }
            }
            catch (Exception ex)
            {

                return Json<object>(new
                {
                    success = false,
#if DEBUG
                    //error = ex.ToString()
                    error = ex.ToString() + "\n Received viewPosition: " + viewPosition
#endif
                });
            }

            return Json<object>(new
            {
                success = false,
            });
        }

        [Route("ClearUser")]
        [HttpPost]
        public void ClearUser()
        {
            //if (GetUserFromSession() != null)
            //{
            //    HttpContext.Current.Session.Clear();
            //}
        }

        [Route("SendContactMessage")]
        [HttpPost]
        public JsonResult<object> SendContactMessage(ContactFormEntity form)
        {
            form.SanitizeAndDefault();
            
            string bodyHtml = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/contactUsEmailHtmlBody.txt"));
            string bodyPlain = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/contactUsEmailPlainBody.txt"));

            // replace name
            bodyHtml = bodyHtml.Replace("@@Name", form.Name);
            bodyPlain = bodyPlain.Replace("@@Name", form.Name);

            // replace email
            bodyHtml = bodyHtml.Replace("@@Email", form.Email);
            bodyPlain = bodyPlain.Replace("@@Email", form.Email);

            // replace message
            bodyHtml = bodyHtml.Replace("@@Message", form.Message);
            bodyPlain = bodyPlain.Replace("@@Message", form.Message);
            
            //Holds message information.
            MailMessage mailMessage = new MailMessage();

            //Add basic information.
            mailMessage.From = new MailAddress("creative@kadho.com");
            mailMessage.To.Add("kadho@kadho.com");
            mailMessage.Subject = "New Message from User on Kadho!";

            //Create two views, one text, one HTML.
            AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(bodyPlain, null, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, null, "text/html");

            //Add two views to message.
            mailMessage.AlternateViews.Add(plainTextView);
            mailMessage.AlternateViews.Add(htmlView);

            // start new thread to send email
            new Thread(() =>
            {
                EmailService emailService = new EmailService();
                emailService.SendEmail(mailMessage);
            }).Start();

            return Json<object>(new
            {
                success = true,
            });
        }

        [Route("SendSupportMessage")]
        [HttpPost]
        public JsonResult<object> SendSupportMessage(ContactFormEntity form)
        {
            form.SanitizeAndDefault();

            string bodyHtml = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/supportEmailHtmlBody.txt"));
            string bodyPlain = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/supportEmailPlainBody.txt"));

            // replace name
            bodyHtml = bodyHtml.Replace("@@Name", form.Name);
            bodyPlain = bodyPlain.Replace("@@Name", form.Name);

            // replace email
            bodyHtml = bodyHtml.Replace("@@Email", form.Email);
            bodyPlain = bodyPlain.Replace("@@Email", form.Email);

            // replace message
            bodyHtml = bodyHtml.Replace("@@Message", form.Message);
            bodyPlain = bodyPlain.Replace("@@Message", form.Message);

            //Holds message information.
            MailMessage mailMessage = new MailMessage();

            //Add basic information.
            mailMessage.From = new MailAddress("creative@kadho.com");
            mailMessage.To.Add("kadho@kadho.com");
            mailMessage.Subject = "Support Message from User on Kadho!";

            //Create two views, one text, one HTML.
            AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(bodyPlain, null, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, null, "text/html");

            //Add two views to message.
            mailMessage.AlternateViews.Add(plainTextView);
            mailMessage.AlternateViews.Add(htmlView);

            // start new thread to send email
            new Thread(() =>
            {
                EmailService emailService = new EmailService();
                emailService.SendEmail(mailMessage);
            }).Start();

            return Json<object>(new
            {
                success = true,
            });
        }

        [Route("SaveUserSettings")]
        [HttpPost]
        public async Task<JsonResult<object>> SaveUserSettings(AccountSettingFormEntity form)
        {
            try
            {
                // padding
                form.Session = form.Session.PadRight(form.Session.Length + (4 - form.Session.Length % 4) % 4, '=');

                string unencryptedLink = EncryptionUtility.AESDecrypt(form.Session, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
                // split at : 
                // Format email:userid
                string[] split = unencryptedLink.Split(':');
                EarlyAccessService eaService = new EarlyAccessService();
                KadhoEarlyAccessUser u = await eaService.GetUser(split[0], split[1]);

                if (u != null)
                {
                    u.Subscribed = form.BetaNews;
                    await u.ParseObject.SaveAsync();

                    return Json<object>(new
                    {
                        success = true,
                        user = new
                        {
                            name = u.Name,
                            email = u.Email,
                            positionInLine = u.WaitListPosition,
                            referCode = u.UserId,
                            subscribed = u.Subscribed
                        }
                    });
                }
            }
            catch
            {
                return Json<object>(new
                {
                    success = false,
                });
            }

            return Json<object>(new
            {
                success = false,
            });
        }

        #region Private helper methods

        /// <summary>
        /// Send signup email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        private void SendSignupEmail(string email, string userId)
        {
            string bodyHtml = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/emailHtmlBody.txt"));
            string bodyPlain = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/App_Data/emailPlainBody.txt"));

            // share url
            string shareUrl = "http://www.kadho.com/?ref=" + HttpUtility.UrlEncode(userId);
            bodyHtml = bodyHtml.Replace("@@ShareUrl", "<a href=\"" + shareUrl + "\">" + shareUrl + "</a>");
            bodyPlain = bodyPlain.Replace("@@ShareUrl", shareUrl);

            // Generate AES encrypted links of the email:userId
            string encryptedLink = EncryptionUtility.AESEncrypt(email + ":" + userId, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
            // view position url
            string checkUrl = "http://www.kadho.com/?view=" + HttpUtility.UrlEncode(encryptedLink);
            bodyHtml = bodyHtml.Replace("@@CheckUrl", "<a href=\"" + checkUrl + "\">here</a>");
            bodyPlain = bodyPlain.Replace("@@CheckUrl", checkUrl);

            // unsubscribe url
            string unsubscribeUrl = "http://www.kadho.com/unsubscribe/?id=" + HttpUtility.UrlEncode(encryptedLink);
            bodyHtml = bodyHtml.Replace("@@UnsubscribeUrl", "<a href=\"" + unsubscribeUrl + "\">Unsubscribe here</a>");
            bodyPlain = bodyPlain.Replace("@@UnsubscribeUrl", unsubscribeUrl);

            //Holds message information.
            MailMessage mailMessage = new MailMessage();

            //Add basic information.
            mailMessage.From = new MailAddress("creative@kadho.com");
            mailMessage.To.Add(email);
            mailMessage.Subject = "Thanks for your interest in Kadho!";

            //Create two views, one text, one HTML.
            AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(bodyPlain, null, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, null, "text/html");

            //Add two views to message.
            mailMessage.AlternateViews.Add(plainTextView);
            mailMessage.AlternateViews.Add(htmlView);

            // start new thread to send email
            new Thread(() =>
            {
                EmailService emailService = new EmailService();
                emailService.SendEmail(mailMessage);
            }).Start();
        }

        /// <summary>
        /// Extract ages from the string split by comma and spaces.
        /// </summary>
        /// <param name="ages"></param>
        /// <returns></returns>
        private List<int> GetChildrenAge(string ages)
        {
            List<int> cAge = new List<int>();

            if (string.IsNullOrWhiteSpace(ages))
                return cAge;

            // split by comma
            string[] split = Regex.Split(ages, ",");
            foreach (string s in split)
            {
                int a = 0;
                if (Int32.TryParse(s, out a))
                    cAge.Add(a);
            }

            // split by space
            split = Regex.Split(ages, " ");
            foreach (string s in split)
            {
                int a = 0;
                if (Int32.TryParse(s, out a))
                    cAge.Add(a);
            }

            return cAge;
        }

        /// <summary>
        /// Parse the zip.
        /// </summary>
        /// <param name="zip"></param>
        /// <returns></returns>
        private int GetZip(string zip)
        {
            int pZ = 0;
            Int32.TryParse(zip, out pZ);
            return pZ;
        }

        #endregion
    }
}