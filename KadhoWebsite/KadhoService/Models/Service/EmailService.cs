﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace KadhoService.Models.Service
{
    public class EmailService
    {
        private static string awsSESUsername = "AKIAJDILMH3AGBSJJUSQ";
        private static string awsSESPassword = "Aujw/vx1ksG7WRZY9gB/2wK3EchAViRS1BnmnrsdbzpN";
        private static string awsSESServer = "email-smtp.us-west-2.amazonaws.com";

        public SmtpClient SMTPServer { get; set; }

        public EmailService()
        {
            // Send email via AWS SES
            SMTPServer = new SmtpClient
            {
                Host = awsSESServer,
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(awsSESUsername, awsSESPassword)
            };
        }

        /// <summary>
        /// Send the email via smtp to AWS SES.
        /// Debug: bcc to creative@kadho.com
        /// </summary>
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        public bool SendEmail(MailMessage mailMessage)
        {
            // bcc to creative@kadho.com for debugging?
            mailMessage.Bcc.Add(new MailAddress("creative@kadho.com"));

            try
            {
                SMTPServer.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}