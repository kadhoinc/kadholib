﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KadhoService.Models.Entity.EarlyAccess
{
    public class SignupEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }
        public string ChildrenAge { get; set; }
        public string Referrer { get; set; }
    }
}