﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KadhoService.Models.Entity.EarlyAccess
{
    public class AccountSettingFormEntity
    {
        public string Session { get; set; }
        public bool BetaNews { get; set; }
    }
}