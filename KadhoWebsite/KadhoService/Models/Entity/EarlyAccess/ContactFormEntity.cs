﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Security.Application;

namespace KadhoService.Models.Entity.EarlyAccess
{
    public class ContactFormEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// Sanitize all input by html encoding and provide default values where null.
        /// </summary>
        public void SanitizeAndDefault()
        {
            if (!string.IsNullOrEmpty(Name))
                Name = HttpUtility.HtmlEncode(Name);
            else
                Name = "*No name listed*";

            if (!string.IsNullOrEmpty(Email))
                Email = HttpUtility.HtmlEncode(Email);
            else
                Email = "*No email listed";

            if (!string.IsNullOrEmpty(Message))
                Message = HttpUtility.HtmlEncode(Message);
            else
                Message = "*No message attached*";
        }
    }
}