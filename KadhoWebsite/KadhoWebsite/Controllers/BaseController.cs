﻿using KadhoWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KadhoWebsite.Models.Entity;

namespace KadhoWebsite.Controllers
{
    public class BaseController : Controller
    {
        public KadhoEarlyAccessUser User { get; set; }

        public BaseController()
        {
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["User"] != null && Session["User"] is KadhoEarlyAccessUser)
            {
                User = (KadhoEarlyAccessUser)Session["User"];
            }

 	         base.OnActionExecuting(filterContext);
        }
    }
}