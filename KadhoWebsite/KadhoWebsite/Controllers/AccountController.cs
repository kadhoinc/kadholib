﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using KadhoLib;
using KadhoLib.Account;
using KadhoLib.Utility;
using KadhoWebsite.Models.Entity;
using KadhoWebsite.Models.Service;
using Parse;
using KadhoLib.Parse;
using System.Threading;

namespace KadhoWebsite.Controllers
{
    public class AccountController : BaseController
    {
        #region AES Encryption consts

        private const string aesPassPhrase = "rrwgb8gsak70hbog";
        private const string aesSaltValue = "ym3v9jl2q0zf56b2";
        private const string aesInitVector = "m0239arrpahgm1h7";

        #endregion

        //[HttpGet]
        //public async Task<ActionResult> Unsubscribe(string code)
        //{
        //    try
        //    {
        //        string unencryptedLink = EncryptionUtility.AESDecrypt(code, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
        //        // split at : 
        //        // Format email:userid
        //        string[] split = unencryptedLink.Split(':');
        //        EarlyAccessService eaService = new EarlyAccessService();

        //        // unsubscribe user
        //        KadhoEarlyAccessUser u = await eaService.GetUser(split[0], split[1]);
        //        if (u != null)
        //        {
        //            u.Subscribed = false;
        //            await u.ParseObject.SaveAsync();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return RedirectToAction("Index", "App", new { unsubscribe = true });
        //}

        [HttpPost]
        public async Task<JsonResult> SignupEarlyAccess(string name = null, string email = null, string zip = null, string childrenAge = null, string referrer = "")
        {
            EarlyAccessService eaService = new EarlyAccessService();

            // trim inputs
            if (!string.IsNullOrWhiteSpace(name))
                name = name.Trim();
            // lower case email
            if (!string.IsNullOrWhiteSpace(email))
                email = email.Trim().ToLower();

            KadhoEarlyAccessUser u = new KadhoEarlyAccessUser()
            {
                Name = name,
                Email = email,
                Zip = GetZip(zip),
                ChildrenAge = GetChildrenAge(childrenAge),
                Subscribed = true,
                Referrer = referrer,
            };

            try
            {
                EarlyAccessService.SignupResponse response = await eaService.Signup(u);

                switch (response.SRType)
                {
                    case EarlyAccessService.SignupResponse.SignupResponseType.Success:
                        {
                            User = u;
                            Session["User"] = User;

                            // new thread for sending email
                            new Thread(() =>
                                {
                                    SendEmail(User.Email, User.UserId);
                                }).Start();

                            return Json(new
                            {
                                success = true,
                                message = response.Message,
                                user = new
                                {
                                    name = User.Name,
                                    email = User.Email,
                                    positionInLine = User.WaitListPosition,
                                }
                            });
                        }
                    case EarlyAccessService.SignupResponse.SignupResponseType.FailEmailExist:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidEmail:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidName:
                    case EarlyAccessService.SignupResponse.SignupResponseType.InvalidZip:
                        {
                            return Json(new
                            {
                                success = false,
                                message = response.SRType.ToString(),
                            });
                        }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                string errStr = ExceptionUtility.FlattenException(ex, true, true);
                Console.WriteLine(errStr);
                return Json(new
                {
                    success = false,
                    message = "Error"
                });
#else                
                return Json(new
                {
                    success = false,
                    message = "Error",
                }, JsonRequestBehavior.AllowGet);
#endif
            }
            
            // default error
            return Json(new
            {
                success = false,
                message = "Error",
            }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public async Task<JsonResult> ResendEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return Json(
                    new
                    {
                        message = "ResendEmailError"
                    });
            }
            
            EarlyAccessService eaService = new EarlyAccessService();

            // trim email
            email = email.Trim().ToLower();

            // query parse to see if email exists
            if (await eaService.CanResendEmail(email))
            {
                await eaService.UpdateEmailResentTime(email, DateTime.Now);
                string userId = await eaService.GetUserIdByEmail(email);

                // new thread for sending email
                new Thread(() =>
                {
                    SendEmail(email, userId);
                }).Start();
                
                return Json(
                    new
                    {
                        message = "ResendEmailSuccess"
                    });
            }

            return Json(
                new
                {
                    message = "RecentlySentEmail"
                });
        }

        [HttpGet]
        public JsonResult GetCurrentUser()
        {
            if (User != null)
            {
                return Json(
                    new
                    {
                        name = User.Name,
                        email = User.Email,
                        positionInLine = User.WaitListPosition,
                    }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { fail = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetUserViewPosition(string viewPosition)
        {
            try
            {
                string unencryptedLink = EncryptionUtility.AESDecrypt(viewPosition, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
                // split at : 
                // Format email:userid
                string[] split = unencryptedLink.Split(':');
                EarlyAccessService eaService = new EarlyAccessService();
                KadhoEarlyAccessUser u = await eaService.GetUser(split[0], split[1]);

                if (u != null)
                {
                    User = u;
                    Session["User"] = User;

                    return Json(new
                    {
                        success = true,
                        user = new
                        {
                            name = User.Name,
                            email = User.Email,
                            positionInLine = User.WaitListPosition,
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

            }

            return Json(new
            {
                success = false,
            }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public void ClearUser()
        {
            if (User != null)
            {
                User = null;
                Session.Clear();
            }
        }

        #region Private helper methods

        private void SendEmail(string email, string userId)
        {
            string bodyHtml = System.IO.File.ReadAllText(Server.MapPath(@"~/App_Data/emailHtmlBody.txt"));
            string bodyPlain = System.IO.File.ReadAllText(Server.MapPath(@"~/App_Data/emailPlainBody.txt"));
            
            // share url
            string shareUrl = "http://www.kadho.com/app/?ref=" + Url.Encode(userId);
            bodyHtml = bodyHtml.Replace("@@ShareUrl", "<a href=\"" + shareUrl + "\">" + shareUrl + "</a>");
            bodyPlain = bodyPlain.Replace("@@ShareUrl", shareUrl);

            // Generate AES encrypted links of the email:userId
            string encryptedLink = EncryptionUtility.AESEncrypt(email + ":" + userId, aesPassPhrase, aesSaltValue, "SHA1", 2, aesInitVector, 128);
            // view position url
            string checkUrl = "http://www.kadho.com/app/?view=" + Url.Encode(encryptedLink);
            bodyHtml = bodyHtml.Replace("@@CheckUrl", "<a href=\"" + checkUrl + "\">here</a>");
            bodyPlain = bodyPlain.Replace("@@CheckUrl", checkUrl);

            // unsubscribe url
            string unsubscribeUrl = "http://www.kadho.com/app/unsubscribe/?id=" + Url.Encode(encryptedLink);
            bodyHtml = bodyHtml.Replace("@@UnsubscribeUrl", "<a href=\"" + unsubscribeUrl + "\">Unsubscribe here</a>");
            bodyPlain = bodyPlain.Replace("@@UnsubscribeUrl", unsubscribeUrl);
            
            //Holds message information.
            MailMessage mailMessage = new MailMessage();

            //Add basic information.
            mailMessage.From = new MailAddress("creative@kadho.com");
            mailMessage.To.Add(email);
            mailMessage.Subject = "Thanks for your interest in Kadho!";

            //Create two views, one text, one HTML.
            AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(bodyPlain, null, "text/plain");
            AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(bodyHtml, null, "text/html");
            
            //Add two views to message.
            mailMessage.AlternateViews.Add(plainTextView);
            mailMessage.AlternateViews.Add(htmlView);

            // Send email via AWS SES
            SmtpClient SMTPServer = new SmtpClient
            {
                Host = "email-smtp.us-west-2.amazonaws.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("AKIAJDILMH3AGBSJJUSQ", "Aujw/vx1ksG7WRZY9gB/2wK3EchAViRS1BnmnrsdbzpN")
            };

            try
            {
                SMTPServer.Send(mailMessage);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Extract ages from the string split by comma and spaces.
        /// </summary>
        /// <param name="ages"></param>
        /// <returns></returns>
        private List<int> GetChildrenAge(string ages)
        {
            List<int> cAge = new List<int>();

            if (string.IsNullOrWhiteSpace(ages))
                return cAge;

            // split by comma
            string[] split = Regex.Split(ages, ",");
            foreach (string s in split)
            {
                int a = 0;
                if (Int32.TryParse(s, out a))
                    cAge.Add(a);
            }

            // split by space
            split = Regex.Split(ages, " ");
            foreach (string s in split)
            {
                int a = 0;
                if (Int32.TryParse(s, out a))
                    cAge.Add(a);
            }

            return cAge;
        }

        /// <summary>
        /// Parse the zip.
        /// </summary>
        /// <param name="zip"></param>
        /// <returns></returns>
        private int GetZip(string zip)
        {
            int pZ = 0;
            Int32.TryParse(zip, out pZ);
            return pZ;
        }

        #endregion
    }
}