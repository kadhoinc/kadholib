﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KadhoWebsite.Controllers
{
    public class TemplateController : Controller
    {
        public ActionResult Main() { return View(); }

        public ActionResult FAQ() { return View(); }

        public ActionResult About() { return View(); }

        public ActionResult Science() { return View(); }

        public ActionResult Unsubscribe() { return View(); }
	}
}