﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;
using System.Threading.Tasks;
using KadhoWebsite.Models.Entity;
using KadhoLib.Parse;
using KadhoLib.Utility;

namespace KadhoWebsite.Models.Service
{
    public class EarlyAccessService
    {
        #region Constructor

        /// <summary>
        /// Create the service and connect to the parse.
        /// </summary>
        public EarlyAccessService()
        {
            ParseConnector.ConnectKadho();
        }

        #endregion

        /// <summary>
        /// Signup the user. Returns a response.
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public async Task<SignupResponse> Signup(KadhoEarlyAccessUser u)
        {
            // check if valid email
            if (!RegexUtility.IsValidEmail(u.Email))
            {
                return new SignupResponse()
                {
                    Message = "Invalid email.",
                    SRType = SignupResponse.SignupResponseType.InvalidEmail
                };
            }

            // check valid zip for USA, 5 digits
            if (u.Zip.ToString().Length != 5)
            {
                return new SignupResponse()
                {
                    Message = "Invalid zip.",
                    SRType = SignupResponse.SignupResponseType.InvalidZip
                };
            }

            // make sure name is not blank
            if (string.IsNullOrWhiteSpace(u.Name))
            {
                return new SignupResponse()
                {
                    Message = "Invalid name.",
                    SRType = SignupResponse.SignupResponseType.InvalidName
                };
            }

            // Check for existing user
            if (await EmailUsedForEarlyAccess(u.Email))
            {
                return new SignupResponse()
                {
                    Message = "Email has already been used signed up!",
                    SRType = SignupResponse.SignupResponseType.FailEmailExist
                };
            }
                                    
            // get next position in line
            Random r = new Random();
            int posIncrement = r.Next(1, 45);
            int positionInLine = await GetNextSignupPosition(posIncrement);
            u.WaitListPosition = positionInLine;

            // check referrer
            await CheckReferrer(u);

            // get a unique id
            u.UserId = await CreateUniqueId();
            
            // save user to the signup line
            u.LastTimeEmailSent = DateTime.Now;            
            await u.ParseObject.SaveAsync();

            return new SignupResponse()
            {
                Message = "Signup successful!",
                SRType = SignupResponse.SignupResponseType.Success
            };
        }

        /// <summary>
        /// Get the next Signup position.
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetNextSignupPosition(int increment)
        {
            // add the user to the signup line
            var querySignupCount = ParseObject.GetQuery("KadhoSignupCount");
            ParseObject result = await querySignupCount.FirstOrDefaultAsync();
            int positionInLine = 0;
            if (result != null)
            {
                positionInLine = result.Get<int>("signUpPosition") + increment;

                // increment
                result.Increment("signUpPosition", increment);
                result.Increment("signUpCount");

                // save the increment
                await result.SaveAsync();
            }
            else // create new
            {
                ParseObject newCount = new ParseObject("KadhoSignupCount");
                positionInLine += increment;
                newCount["signUpPosition"] = positionInLine;
                newCount["signUpCount"] = 1;
                await newCount.SaveAsync();
            }
            return positionInLine;
        }

        /// <summary>
        /// Check if the email has already been signed up for early access.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<bool> EmailUsedForEarlyAccess(string email)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("email", email);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            return result != null;
        }
        
        /// <summary>
        /// Check if the early access email can be resent.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<bool> CanResendEmail(string email)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("email", email);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            if (result != null)
            {
                DateTime lastSent = result.Get<DateTime>("lastTimeEmailSent");
                // minimum 1 hour between resending emails to prevent spamming
                return (DateTime.Now - lastSent).TotalHours >= 1.0;

            }
            return false;
        }

        public async Task CheckReferrer(KadhoEarlyAccessUser u)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("userId", u.Referrer);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            if (result != null)
            {
                KadhoEarlyAccessUser referrer = new KadhoEarlyAccessUser(result);
                referrer.ParseObject.Increment("waitListPosition", -1);
                await referrer.ParseObject.SaveAsync();
            }
            else // not found
                u.Referrer = string.Empty;
        }

        /// <summary>
        /// Get the user id by email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<string> GetUserIdByEmail(string email)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("email", email);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            if (result != null)
            {
                return result.Get<string>("userId");

            }
            return string.Empty;
        }

        /// <summary>
        /// Update the email resent time.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public async Task UpdateEmailResentTime(string email, DateTime dateTime)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("email", email);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            if (result != null)
            {
                result["lastTimeEmailSent"] = dateTime;
                await result.SaveAsync();
            }
        }

        /// <summary>
        /// Create a unique user id for wait list users.
        /// </summary>
        /// <returns></returns>
        public async Task<string> CreateUniqueId()
        {
            string rs = "abcdefghijklmnopqrstuvwxyz0123456789";
            string id = string.Empty;
            bool unique = false;
            Random rand = new Random();
            while (!unique)
            {
                id = string.Empty;
                for (int i = 0; i < 6; i++)
                    id += rs[rand.Next(0, rs.Length)];

                var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine").WhereEqualTo("userId", id);
                ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
                if (result == null)
                    unique = true;
            }
            return id;
        }

        /// <summary>
        /// Get the user with the email and userid.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<KadhoEarlyAccessUser> GetUser(string email, string userId)
        {
            var queryCheckSignup = ParseObject.GetQuery("KadhoEarlyAccessLine")
                .WhereEqualTo("email", email).WhereEqualTo("userId", userId);
            ParseObject result = await queryCheckSignup.FirstOrDefaultAsync();
            if (result != null)
                return new KadhoEarlyAccessUser(result);
            return null;
        }

        public class SignupResponse
        {
            public enum SignupResponseType
            {
                Success,
                FailEmailExist,
                InvalidEmail,
                InvalidZip,
                InvalidName,
            }

            /// <summary>
            /// Signup Response type
            /// </summary>
            public SignupResponseType SRType { get; set; }

            /// <summary>
            /// Signup response message
            /// </summary>
            public string Message { get; set; }
        }
    }
}