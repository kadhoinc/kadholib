﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KadhoLib.Parse;

namespace KadhoWebsite.Models.Entity
{
    public class KadhoEarlyAccessUser : ParseObjectWrapper
    {
        #region Properties

        /// <summary>
        /// Name of user
        /// </summary>
        public string Name
        {
            get { return ParseObject.Get<string>("name"); }
            set { AddValue("name", value); }
        }

        /// <summary>
        /// List of children ages
        /// </summary>
        public List<int> ChildrenAge
        {
            get { return GetInt32List("childrenAge"); }
            set { AddValue("childrenAge", value); }
        }

        /// <summary>
        /// Zip code.
        /// </summary>
        public int Zip
        {
            get { return ParseObject.Get<int>("zip"); }
            set { AddValue("zip", value); }
        }

        /// <summary>
        /// Email address
        /// </summary>
        public string Email
        {
            get { return ParseObject.Get<string>("email"); }
            set { AddValue("email", value); }
        }

        /// <summary>
        /// Position in wait list.
        /// </summary>
        public int WaitListPosition
        {
            get { return ParseObject.Get<int>("waitListPosition"); }
            set { AddValue("waitListPosition", value); }
        }

        /// <summary>
        /// Last datetime email was sent.
        /// </summary>
        public DateTime LastTimeEmailSent
        {
            get { return ParseObject.Get<DateTime>("lastTimeEmailSent"); }
            set { AddValue("lastTimeEmailSent", value); }
        }

        /// <summary>
        /// Unique user ID
        /// </summary>
        public string UserId
        {
            get { return ParseObject.Get<string>("userId"); }
            set { AddValue("userId", value); }
        }

        /// <summary>
        /// Subscribed to email notifications.
        /// </summary>
        public bool Subscribed
        {
            get { return ParseObject.Get<bool>("subscribed"); }
            set { AddValue("subscribed", value); }
        }

        /// <summary>
        /// Referrer userID.
        /// </summary>
        public string Referrer
        {
            get { return ParseObject.Get<string>("referrer"); }
            set { AddValue("referrer", value); }
        }

        #endregion

        #region Constructor

        public KadhoEarlyAccessUser()
            : base("KadhoEarlyAccessLine")
        {

        }

        public KadhoEarlyAccessUser(ParseObject p)
            : base(p)
        {

        }

        #endregion
    }
}