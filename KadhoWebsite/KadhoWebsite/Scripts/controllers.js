var app = angular.module('myApp');

app.controller('IndexCtrl', ['$scope', 'BackgroundService',
  function ($scope, BackgroundService) {
      $scope.indexClass = "splash";
      $scope.footerClass = "home-footer";

      // set the random background
      var a = (new Date().getDate() % 3) + 1;
      $scope.selectedBg = "../Content/images/bg" + a + ".jpg";

      BackgroundService.onBackgroundChange(function () {
          var d = (new Date().getDate() % 3) + 1;

          $scope.indexClass = "splash";
          $scope.selectedBg = "../Content/images/bg" + d + ".jpg";
      });


      $scope.$on('hideBackground', function () {
          if ($scope.selectedBg != "") {
              $scope.prevBg = $scope.selectedBg;
              $scope.selectedBg = "";
          }
      });

      $scope.$on('showBackground', function () {
          if ($scope.prevBg != null) {
              $scope.selectedBg = $scope.prevBg;
              $scope.prevBg = null;
          }
      });


      $scope.$on('fixedFooter', function () {
          $scope.footerClass = "home-footer";
      });

      $scope.$on('dynamicFooter', function () {
          $scope.footerClass = "home-footer-dynamic";
      });
  }
]);


app.controller('MainCtrl', ['$scope', 'UserService', '$routeParams', 'BackgroundService',
  function ($scope, UserService, $routeParams, BackgroundService) {
      $scope.new_user = {};
      $scope.displayCompleteSignupModal = false;
      $scope.buttonAttempt = false;
      $scope.indexClass = "splash";
      $scope.$emit("showBackground");
      $scope.$emit("fixedFooter");
      
      BackgroundService.setBackground();

      $scope.isPlaying = false;

      $scope.pause = function () {
          $scope.isPlaying = false;
      };

      $scope.play = function () {
          $scope.isPlaying = true;
      };

      $scope.clearUser = function () {
          UserService.clearUser();
          $scope.user = null;
          $scope.doDisplayCompleteSignupModal();
      }

      if ($routeParams.view && !$scope.user) {
          console.log($routeParams.view);
          UserService.getUserViewPosition($routeParams.view)
              .success(function (data) {
                  if (!data.success) {
                      $scope.signInAttempted = true;
                      console.log("no login");
                  }
                  else {
                      UserService.setCurrentUser(data.user);
                      console.log(UserService.getCurrentUser());
                      $scope.signInAttempted = true;
                      $scope.user = UserService.getCurrentUser();
                  }

              })
              .error(function (data, status, headers, config) {
                  $scope.signInAttempted = true;
                  console.log("error");
              });

      }
      else {
          UserService.getCurrentUser()
              .success(function (data) {
                  if (data.fail) {
                      $scope.signInAttempted = true;
                      console.log("no login");
                  }
                  else {
                      UserService.setCurrentUser(data);
                      console.log(UserService.getCurrentUser());
                      console.log(data.firstName);
                      $scope.signInAttempted = true;
                      $scope.user = UserService.getCurrentUser();
                  }

              })
              .error(function (data, status, headers, config) {
                  $scope.signInAttempted = true;
                  console.log("error");
              });
      }

      $scope.signedIn = function (oauth) {
          /*
        UserService.setCurrentUser(oauth).then(function(user) {
            $scope.signInAttempted = true;
            $scope.user = user;
          },
          function(reason) {
            $scope.signInAttempted = true;
          });
          */
      };

      $scope.doDisplayCompleteSignupModal = function () {
          $scope.displayCompleteSignupModal = true;
      };

      $scope.hideCompleteSignupModal = function () {
          $scope.displayCompleteSignupModal = false;
      };

      $scope.$on('completeSignup', function () {
          $scope.hideCompleteSignupModal();
          $scope.signInAttempted = true;
          $scope.user = UserService.getCurrentUser();

      });


      $scope.$on('showSignup', function () {
          $scope.doDisplayCompleteSignupModal();

      });
  }
]);

app.controller('SignupFormController', ['$scope',
  function ($scope) {
      $scope.submit = function () {
          console.log("started signup");
          $scope.$emit("showSignup");
      };
  }
]);

app.controller('ConfirmFormController', ['$scope', '$sce', '$routeParams', 'UserService',
    function ($scope, $sce, $routParams, UserService) {

        $scope.resendEmail = function () {
            UserService.resendEmail($scope.new_user.email)
                .success(function (data) {
                    $scope.signupMessage = data.message;
                })
                .error(function (data, status, headers, config) {
                    $scope.signupMessage = "ResendEmailError";
                    console.log("resendEmail error");
                });
        };

        $scope.submit = function () {
            $scope.buttonAttempt = true;
            console.log("ConfirmFormController signup");
            console.log($scope.new_user);

            if ($scope.confirmForm.$valid) {
                $scope.disableSubmit = true;
                if ($routParams.ref)
                    $scope.new_user.referrer = $routParams.ref;
                UserService.signup($scope.new_user)
                  .success(function (data, status, headers, config) {
                      $scope.disableSubmit = false;
                      if (data.success) {
                          console.log("success");
                          console.log(data.user);
                          console.log(data.message);
                          UserService.setCurrentUser(data.user);
                          $scope.user = UserService.getCurrentUser();
                          console.log("Signup successful! Welcome " + $scope.user.name + "! You are #" + $scope.user.positionInLine + " in line!");
                          $scope.signupMessage = null;
                          $scope.$emit('completeSignup');
                      }
                      else {
                          $scope.signupMessage = data.message;
                          console.log("signup failed: " + $scope.signupMessage);
                      }
                  })
                  .error(function (data, status, headers, config) {
                      console.log(data);
                      $scope.disableSubmit = false;
                  });
            }
        };
    }
]);


app.controller('AboutCtrl', ['$scope', 'UserService', '$routeParams', 'BackgroundService',
  function ($scope, UserService, $routeParams, BackgroundService) {

      console.log("about controller");
      $scope.$emit("hideBackground");
      $scope.$emit("dynamicFooter");
  }
]);

app.controller('FAQCtrl', ['$scope', 'UserService', '$routeParams', 'BackgroundService',
  function ($scope, UserService, $routeParams, BackgroundService) {

      console.log("about controller");
      $scope.$emit("hideBackground");
      $scope.$emit("dynamicFooter");
  }
]);