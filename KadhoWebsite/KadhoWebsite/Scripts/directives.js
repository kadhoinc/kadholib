var app = angular.module('myApp.directives', []);

app.directive('autoFocus', function ($timeout) {
    return {
        restrict: 'AC',
        link: function (scope, element) {
            $timeout(function () {
                element.focus();
                console.log('focus', element);
            }, 0);
        }
    };
});

app.directive('testDirective', function (BackgroundService) {
    return {
        restrict: 'E',
        template: '',
        replace: true,
        compile: function () {
            console.log("Compiling test-directive");
            return {
                post: function () {
                    BackgroundService.notifyObservers();
                }
            };
        }
    };
});



app.directive('passwordInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {

        }
    }
});




app.directive('googleSignin', function () {
    return {
        restrict: 'A',
        template: '<span id="signinButton"></span>',
        replace: true,
        scope: {
            afterSignin: '&'
        },
        link: function (scope, ele, attrs) {
            attrs.$set('class', 'g-signin');

            attrs.$set('data-clientid',
              attrs.clientId + '.apps.googleusercontent.com');

            var scopes = attrs.scopes || [
              'auth/plus.login',
              'auth/userinfo.email'
            ];

            var scopeUrls = [];

            for (var i = 0; i < scopes.length; i++) {
                scopeUrls.push('https://www.googleapis.com/' + scopes[i]);
            }

            var callbackId = "_googleSigninCallback",
              directiveScope = scope;
            window[callbackId] = function () {
                var oauth = arguments[0];
                directiveScope.afterSignin({
                    oauth: oauth
                });
                window[callbackId] = null;
            };

            attrs.$set('data-callback', callbackId);
            attrs.$set('data-cookiepolicy', 'single_host_origin');
            attrs.$set('data-requestvisibleactions', 'http://schemas.google.com/AddActivity');
            attrs.$set('data-scope', scopeUrls.join(' '));

            (function () {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;
                po.src = 'https://apis.google.com/js/client:plusone.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();
        }
    };
});



//
//app.directive('fbLogin', function() {
//  return {
//    restrict: 'A',
//    template: '<span id="signinButton" ng-click="fbLogin()">Signin</span>',
//    replace: true,
//    scope: {
//      afterSignin: '&',
//      fbLogin: '&'
//    },
//    link: function(scope, ele, attrs) {
//      attrs.$set('class', 'fb-signin');
//
//      console.log(scope);
//    }
//  };;
//});



app.directive('vimeo', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            //Assumes that true means the video is playing
            controlBoolean: '=',
            height: '@',
            width: '@'
        },
        template: '<iframe id="{{id}}" height="{{height}}" width="{{width}}"> {{text}} </iframe>',
        link: function postLink(scope, element, attrs) {
            var url = "//player.vimeo.com/video/" + attrs.vid + "?title=0&byline=0&portrait=0&api=1";
            element.attr('src', url);

            var iframe = element[0];
            var player = $f(iframe);

            scope.$watch('controlBoolean', function () {
                if (scope.controlBoolean) {
                    console.log("playing");
                    player.api('play');
                }
                else {
                    console.log("pausing");
                    player.api('pause');
                }
            });
        }
    };
});