var app = angular.module('myApp.services', []);


app.factory('BackgroundService', function ($q) {
    var service = {
        observerCallbacks: [],
        backgroundImage: '',
        indexClass: '',

        onBackgroundChange: function (callback) {
            service.observerCallbacks.push(callback);
        },
        notifyObservers: function () {
            console.log("Notifying observers of bg change");
            angular.forEach(service.observerCallbacks, function (callback) {
                callback();
            });
        },

        setBackground: function () {
            console.log("setting bg");
            service.indexClass = "splash";
        }
    };
    return service;
});

app.factory('UserService', function ($q, $http) {
    var service = {
        _user: null,
        getCurrentUser: function () {
            if (service._user)
                return service._user;
            return $http.get("http://service.kadho.com/api/earlyaccess/GetCurrentUser/");
        },
        getUserViewPosition: function (viewPosition) {
            return $http.get("http://service.kadho.com/api/earlyaccess/GetUserViewPosition/?viewPosition=" + viewPosition);
        },
        setCurrentUser: function (user) {
            service._user = user;
        },
        signup: function (new_user) {
            if (!new_user.referrer)
                new_user.referrer = "";
            return $http.post("http://service.kadho.com/api/earlyaccess/SignupEarlyAccess",
                {
                    name: new_user.name,
                    email: new_user.email,
                    zip: new_user.zip,
                    childrenAge: new_user.childrenAge,
                    referrer: new_user.referrer
                });
        },
        resendEmail: function (email) {
            return $http.post("http://service.kadho.com/api/earlyaccess/ResendEmail",
                {
                    email: email
                });
        },
        clearUser: function () {
            service._user = null;
            return $http.post("http://service.kadho.com/api/earlyaccess/ClearUser");
        }

    }
    return service;
    /*
  var service = {
    _user: null,
    UsersTable: "Users",
    setCurrentUser: function(u) {
      if (u && !u.error) {
        AWSService.setToken(u.id_token);
        return service.currentUser();
      } else {
        console.log("no user");
        var d = $q.defer();
        d.reject(u.error);
        return d.promise;
      }
    },
    currentUser: function() {
      var d = $q.defer();
      if (service._user) {
        d.resolve(service._user);
      } else {
        AWSService.credentials().then(function() {
          gapi.client.oauth2.userinfo.get()
          .execute(function(e) {
            var email = e.email;
            AWSService.dynamo({
              params: {TableName: service.UsersTable}
            })
            .then(function(table) {
              table.getItem({
                Key: {
                  'User email': {
                    S: email
                  }
                }
              },
                function(err, data) {
                  if (data && Object.keys(data).length == 0) {
                    // User didn't previously exist
                    var itemParams = {
                      Item: {
                        'User email': {S: email},
                        data: {
                          S: JSON.stringify(e)
                        }
                      }
                    };
                    table.putItem(itemParams, function(err, data) {
                      service._user = e;
                      d.resolve(e);
                    })
                  } else if (data) {
                    service._user = JSON.parse(
                      data.Item.data.S
                    );
                    d.resolve(service._user);
                  } else {
                    console.log(err);
                    console.log(data);
                  }
              });
            })
          });
        });
      }
      return d.promise;
    }
  };
  return service;
  */
});

app.provider('AWSService', function() {
  var self = this;
  self.arn = null;
  

  // Set defaults
  AWS.config.region = 'us-east-1';

  self.setRegion = function(region) {
    if (region) AWS.config.region = region;
  }

  self.setArn = function(arn) {
    if (arn) self.arn = arn;
  };

  self.$get = function($q, $cacheFactory) {
    var dynamoCache = $cacheFactory('dynamo'),
      credentialsDefer = $q.defer(),
      credentialsPromise = credentialsDefer.promise;

    return {
      credentials: function() {
        return credentialsPromise;
      },
      setToken: function(token, providerId) {
        var config = {
          RoleArn: self.arn,
          WebIdentityToken: token,
          RoleSessionName: 'web-id'
        }
        if (providerId) {
          config['ProviderId'] = providerId;
        }
        self.config = config;
        AWS.config.credentials =
          new AWS.WebIdentityCredentials(config);
        credentialsDefer
          .resolve(AWS.config.credentials);
      },
      dynamo: function(params) {
        var d = $q.defer();
        credentialsPromise.then(function() {
          var table = dynamoCache.get(JSON.stringify(params));
          if (!table) {
            var table = new AWS.DynamoDB(params);
            dynamoCache.put(JSON.stringify(params), table);
          };
          d.resolve(table);
        });
        return d.promise;
      }
    };  // return
  };  // self.$get
});  // app.provider
