var app = angular.module('myApp',
  ['ngSanitize', 'ngRoute', 'myApp.services', 'myApp.directives', 'ngModal', 'ui.select', 'ui.utils']);


app.config(function(AWSServiceProvider) {
  AWSServiceProvider.setArn('arn:aws:iam::879191953195:role/google-web-role');
});

app.config(function($routeProvider, $locationProvider) {
  $routeProvider.when('/app/', {
    controller: 'MainCtrl',
    templateUrl: '../Template/Main'
  }).when('/app/FAQ', {
      controller: 'FAQCtrl',
      templateUrl: '../Template/FAQ'
  }).when('/app/About', {
      controller: 'AboutCtrl',
      templateUrl: '../Template/About'
  }).when('/app/Science', {
      controller: 'AboutCtrl',
      templateUrl: '../Template/Science'
  }).when('/app/Unsubscribe', {
      controller: 'FAQCtrl',
      templateUrl: '../Template/Unsubscribe'
  }).when('/app/AskAQuestion', {
      controller: 'AskAQuestionCtrl',
      templateUrl: '../Template/AskAQuestion'
  }).when('/Client/', {
      controller: 'MainCtrl',
      templateUrl: '../Template/Main'
  }).otherwise({
    redirectTo: '/Client/'
  });

  // use the HTML5 History API
    $locationProvider.html5Mode(true);
});


window.onLoadCallback = function() {
  // When the document is ready
  angular.element(document).ready(function() {
    // Bootstrap the oauth2 library
    gapi.client.load('oauth2', 'v2', function() {
      // Finally, bootstrap our angular app
      angular.bootstrap(document, ['myApp']);
    });
  });
};
